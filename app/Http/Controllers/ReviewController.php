<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Review;
use Illuminate\Http\Request;
use DB;
class ReviewController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('review.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(request $request)
	{
		$this->validate($request, [
            'name' => 'required',
        ]);
        
        $isactive=$request->isactive?1:0;

        $review = new Review;
        $review->name = $request->name;
        $review->isactive = $isactive;
        $review->save();

        return redirect('admin-ecom/review-title')->with('status', 'Review Added Successfully!');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data=Review::all();
		$json=Review::find($id);
        return view('review.index',['edit'=>$json,'data'=>$data]);
	}
	public function showjson()
    {
        $json =Review::all();
        $retarray=array("data"=>$json,"total"=>count($json));

        return response()->json($retarray);
        //"{\"data\":" . json_encode($json) . ",\"total\":" . count($json) . "}"
    }
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(request $request)
	{
		$this->validate($request,[
            'name'=>'required',
            
        ]);
        
        $isactive=$request->isactive?1:0;
        $id=$request->id;
        $review=Review::find($id);
        $review->name = $request->name;
        $review->isactive=$isactive;
        $review->save();

        //echo 1;

        //\Session::flash('message','Record Successfullu Updated.');
        //    return redirect()->action("ProjectController@index");

        return redirect('admin-ecom/review-title')->with('status', 'Review info Modified successfully!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$json=Review::find($id);
        $json->delete();
        return response()->json(1);
	}
	//review request
	public function ReviewRequest(Request $request) {
         $satisfaction = $request->satisfaction;
        $ids = array();
		foreach($satisfaction as $val)
		{
		$ids[] = (string) $val;
		}
       
        $sibs1 = implode(',', $ids);
        //dd($sibs1);
        $values = array(
            'name' => $request->name,
            'email'=>$request->email,
            'subject'=>$request->subject,
            'feedback'=>$request->feedback,
            'age'=>$request->age,
            'services'=>$request->services,
            'support'=>$request->support,
            'team'=>$request->team,
            'satisfaction'=>$sibs1
        );
        
        $con = DB::table('users_review')->insert($values);
       

         return redirect()->action("IndexController@reviews");
    }
    public function adminReviewRequest() {
        return view('review.review_request')->with('status', 'Contact detail info saved successfully!');
    }
    // public function ReviewEditshowjson($id) {
    //     $json = DB::table('users_review')
    //             ->where('id',$id)
    //             ->first();
    //             //dd($json);
    //     return view('review.review_request', ['data' => $json]);
    // }
    public function Reviewshowjson() {
        $json = DB::table('users_review')->get();

        $retarray = array("data" => $json, "total" => count($json));

        return response()->json($retarray);
        //"{\"data\":" . json_encode($json) . ",\"total\":" . count($json) . "}"
    }
    public function Requestdestroy($id)
	{
		$json=DB::table('users_review')
                ->where('id',$id)
                ->delete();
        return response()->json(1);
	}
}
