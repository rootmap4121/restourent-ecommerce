<?php

namespace App\Http\Controllers;
use App\category;
use App\ProductItem;
use App\PizzaSize;
use App\PizzaFlabour;
use App\ProductOneSubLevel;
use App\ProductTwoSubLevel;
use App\Product;
use App\Discount;
use App\Tax;
use App\SubCategory;
use Session;
use App\Cart;
use App\Customer;
use App\DeliveryAddress;
use App\OrderInfo;
use Auth;
use App\Orders;
use App\OrdersItem;
use Illuminate\Http\Request;

class ProductItemController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $moduleName="";
    private $sdc;
    public function __construct(){ 
        $this->sdc = new MenuPageController(); 
    }

    public function index()
    {
        $category=$this->categoryProduct();
        //$product=Product::all();
        $defultReturn=['category'=>$category];

        if($this->checkCommonDiscount())
        {
            $defultReturn=array_merge($defultReturn,['common'=>$this->checkCommonDiscount()]);
        }

        if($this->checkColNDelDiscount())
        {
            $defultReturn=array_merge($defultReturn,['colndel'=>$this->checkColNDelDiscount()]);
        }        

        if($this->checkTax())
        {
            $defultReturn=array_merge($defultReturn,['tax'=>$this->checkTax()]);
        }

        $orderINfo=OrderInfo::orderBy('id','DESC')->first();
        $defultReturn=array_merge($defultReturn,['orderINfoText'=>$orderINfo]);
        //dd($defultReturn);

        return view('frontend.pages.product.index',$defultReturn);
    }

    public function makePayment(Request $request)
    {
        $cart = Session::has('cart') ? Session::get('cart') : null;
        $defultReturn=['cart'=>$cart];
        if($this->checkCommonDiscount())
        {
            $defultReturn=array_merge($defultReturn,['common'=>$this->checkCommonDiscount()]);
        }

        if($this->checkColNDelDiscount())
        {
            $defultReturn=array_merge($defultReturn,['colndel'=>$this->checkColNDelDiscount()]);
        }        

        if($this->checkTax())
        {
            $defultReturn=array_merge($defultReturn,['tax'=>$this->checkTax()]);
        }

        //dd(csrf_token());

         
        $totalPrice=$defultReturn['cart']->totalPrice;

        if($totalPrice<1)
        {
            return redirect('order-item')->with('error', 'Failed to process order, Please try again.');
        }

        
        $tax_title="Tax";
        $tax_amount=$defultReturn['tax']->tax_amount;
        if(isset($defultReturn['tax']))
        {
            if (strpos($defultReturn['tax']->tax_amount, '%') !== false) {
               $tax_title="Tax (".$defultReturn['tax']->tax_amount.")";
               $tax_amount=(($totalPrice*$defultReturn['tax']->tax_amount)/100);
            }
        }

        $discount_title="Discount";
        $discount_amount=0;

        if(isset($defultReturn['colndel']))
        {

            if($totalPrice > $defultReturn['colndel']->minimum_amount)
            {

                $recType=$defultReturn['cart']->rec;
            
                if(in_array($defultReturn['colndel']->discount_option,array("Delivery","Collection")))
                {
                   // echo $defultReturn['colndel']->discount_option.";".$recType; die();
                    $discount_title="Discount";
                    $discount_amount=0;
                    if($recType=="Collect")
                    {
                        $discount_title="Discount on Collection";
                        $discount_amount=$defultReturn['colndel']->discount_amount;
                        if (strpos($defultReturn['colndel']->discount_amount, '%') !== false) {

                           $discount_title="Discount (".$defultReturn['colndel']->discount_amount.")";
                           $discount_amount=(($totalPrice*$defultReturn['colndel']->discount_amount)/100);
                        }
                    }
                    elseif($recType=="Delivery")
                    {
                        $discount_title="Discount on Delivery";
                        $discount_amount=$defultReturn['colndel']->discount_amount;
                        if (strpos($defultReturn['colndel']->discount_amount, '%') !== false) {
                           $discount_title="Discount (".$defultReturn['colndel']->discount_amount.")";
                           $discount_amount=(($totalPrice*$defultReturn['colndel']->discount_amount)/100);
                        }
                    }
                }
                else
                {
                    $discount_title="Discount";
                    $discount_amount=$defultReturn['colndel']->discount_amount;
                    if (strpos($defultReturn['colndel']->discount_amount, '%') !== false) {
                       $discount_title="Discount (".$defultReturn['colndel']->discount_amount.")";
                       $discount_amount=(($totalPrice*$defultReturn['colndel']->discount_amount)/100);
                    }
                }    
            }
                
        }
        
       // dd($defultReturn['cart']->items);
        $delivery = new DeliveryAddress;
        $delivery->customer_id = Auth::user()->id;
        $delivery->token = csrf_token();
        $delivery->first_name = $defultReturn['cart']->deliveryDetail["name"];
        $delivery->address = $defultReturn['cart']->deliveryDetail["address"];
        $delivery->mobile_phone =$defultReturn['cart']->deliveryDetail["phone"];
        $delivery->email =$defultReturn['cart']->deliveryDetail["email"];
        $delivery->save();

        $pro = new Orders;
        $pro->tracking = csrf_token();
        $pro->cid = Auth::user()->id;
        $pro->invoice_date = date('Y-m-d');
        $pro->due_date = date('Y-m-d');
        $pro->order_status = "Pending";
        $pro->tax_title = $tax_title;
        $pro->tax_amount = $tax_amount;
        $pro->discount_title = $discount_title;
        $pro->discount_amount = $discount_amount;
        $pro->order_total = $totalPrice;
        $pro->save();

        $order_id = $pro->id;


        if(count($defultReturn['cart']->items)>0)
        {
            foreach($defultReturn['cart']->items as $itm):
                //dd($itm);

                $protag = new OrdersItem();
                $protag->pid = $itm['item']->id;
                $protag->order_id = $order_id;
                $protag->tracking = csrf_token();
                $protag->quantity = $itm["qty"];
                $protag->unit_price =$itm['item']->price;
                $protag->tax_rate = 0;
                $protag->tax_amount = 0;
                $protag->row_total = $itm["price"];
                $protag->save();

            endforeach;
        }
        else
        {
            return redirect('order-item')->with('error', 'Failed to process order, Please try again.');
        }

        

        $siteMessage='<h2><strong><span style="color: #ff9900;">Receipt</span></strong></h2>
                        <table style="border: 2px solid #000000; width: 436px;">
                        <tbody>
                        <tr style="height: 32px;">
                        <td style="width: 184px; height: 32px;">Order Time</td>
                        <td style="width: 244px; height: 32px;">&nbsp;'.date('dS M Y, h:i A').'</td>
                        </tr>
                        <tr style="height: 46px;">
                        <td style="width: 428px; height: 46px;" colspan="2">
                        <h3 style="display: block; width: 80%; border-bottom: 3px #000 solid;">
                            <strong>Customer Detail</strong>
                        </h3>
                        </td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Name</td>
                        <td style="width: 244px; height: 18px;">'.$delivery->first_name.'</td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Order Type&nbsp;</td>
                        <td style="width: 244px; height: 18px;">Delivery</td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Payment Type&nbsp;</td>
                        <td style="width: 244px; height: 18px;">Cash</td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Deliver Date&nbsp;</td>
                        <td style="width: 244px; height: 18px;">ASAP</td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Address</td>
                        <td style="width: 244px; height: 18px;">'.$delivery->address.'</td>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Phone</td>
                        <td style="width: 244px; height: 18px;">'.$delivery->mobile_phone.'</td>
                        </tr>
                        </tr>
                        <tr style="height: 18px;">
                        <td style="width: 184px; height: 18px;">&nbsp;Email</td>
                        <td style="width: 244px; height: 18px;">'.$delivery->email.'</td>
                        </tr>
                        <tr style="height: 46px;">
                        <td style="width: 428px; height: 46px;" colspan="2">
                        <h3 style="display: block; width: 80%; border-bottom: 3px #000 solid;">
                            <strong>Order Detail</strong>
                        </h3>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table align="center" width="100%">
                                    <thead>
                                        <tr>
                                            <th align="left">Quantity</th>
                                            <th align="left">Product Name</th>
                                            <th align="right">Price</th>
                                        </tr>
                                        <tr>
                                            <th colspan="3">
                                                <span style="display: block; width: 100%; border-bottom: 3px #000 solid;">&nbsp;
                                                </span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mixxed Plater</td>
                                            <td align="right">$14.95</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3">
                                                <span style="display: block; width: 100%; border-bottom: 3px #000 solid;">&nbsp;
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Sub Total</td>
                                            <td align="right">$28.90</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Tax</td>
                                            <td align="right">$28.90</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Discount</td>
                                            <td align="right">$28.90</td>
                                        </tr>
                                        <tr>
                                            <th colspan="3">
                                                <span style="display: block; width: 100%; border-bottom: 3px #000 solid;">&nbsp;
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Net Payable</td>
                                            <td align="right">$28.90</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                        <p>Kind Regards, '.$this->sdc->SiteName.'&nbsp;</p>
                        <p>&nbsp;</p>';
       
        $this->sdc->initMail(
            $delivery->email,
            'Online Order Receipt - '.$this->sdc->SiteName,
            $this->sdc->TableUserOrder($delivery->first_name).'<br>'.$siteMessage,$this->sdc->TableReservationAdminEmail);

        echo 1;
        exit();
        
        return redirect('complete-payment')->with('status', 'Order placed successfully, Your order will confirmed soon.!');

    }

    public function completePayment()
    {
        $category=$this->categoryProduct();
        //$product=Product::all();
        $defultReturn=['category'=>$category];

        if($this->checkCommonDiscount())
        {
            $defultReturn=array_merge($defultReturn,['common'=>$this->checkCommonDiscount()]);
        }

        if($this->checkColNDelDiscount())
        {
            $defultReturn=array_merge($defultReturn,['colndel'=>$this->checkColNDelDiscount()]);
        }        

        if($this->checkTax())
        {
            $defultReturn=array_merge($defultReturn,['tax'=>$this->checkTax()]);
        }

        $orderINfo=OrderInfo::orderBy('id','DESC')->first();
        $defultReturn=array_merge($defultReturn,['orderINfoText'=>$orderINfo]);
        //dd($defultReturn);

        return view('frontend.pages.checkout.complete-payment',$defultReturn);
    }

    public function userDashboard()
    {
        $category=$this->categoryProduct();
        //$product=Product::all();
        $defultReturn=['category'=>$category];

        if($this->checkCommonDiscount())
        {
            $defultReturn=array_merge($defultReturn,['common'=>$this->checkCommonDiscount()]);
        }

        if($this->checkColNDelDiscount())
        {
            $defultReturn=array_merge($defultReturn,['colndel'=>$this->checkColNDelDiscount()]);
        }        

        if($this->checkTax())
        {
            $defultReturn=array_merge($defultReturn,['tax'=>$this->checkTax()]);
        }

        $orderINfo=OrderInfo::orderBy('id','DESC')->first();
        $defultReturn=array_merge($defultReturn,['orderINfoText'=>$orderINfo]);
        //dd($defultReturn);

        return view('frontend.pages.user.dashboard',$defultReturn);
    }

    private function checkCommonDiscount()
    {
        $chk=Discount::where('discount_status','Active')
                     ->where('discount_option','Common')
                     ->count();
        if($chk>0)
        {
            $data=Discount::select(
                            'id',
                            'minimum_amount',
                            'discount_option',
                            'discount_type',
                            \DB::Raw("CASE WHEN discount_type='Fixed' THEN discount_amount 
                            ELSE CONCAT(discount_amount,'%') END as discount_amount"),
                            'message',
                            'discount_status',
                            'created_at'
                            )
                          ->where('discount_status','Active')
                          ->where('discount_option','Common')
                          ->first();
            //dd($data);
            return $data;
        }
    }

    private function checkColNDelDiscount()
    {
        $chk=Discount::where('discount_status','Active')
                     ->whereIn('discount_option', ['Delivery','Collection'])
                     ->count();
        //dd($chk);
        if($chk>0)
        {
            $data=Discount::select(
                            'id',
                            'minimum_amount',
                            'discount_option',
                            'discount_type',
                            \DB::Raw("CASE WHEN discount_type='Fixed' THEN discount_amount 
                            ELSE CONCAT(discount_amount,'%') END as discount_amount"),
                            'message',
                            'discount_status',
                            'created_at'
                            )
                          ->where('discount_status','Active')
                          ->whereIn('discount_option', ['Delivery','Collection'])
                          ->first();
            //dd($data);
            return $data;
        }
    }
    
    private function checkTax()
    {
        $chk=Tax::where('tax_status','Active')
                ->orderBy('id','DESC')
                ->count();
        //dd($chk);



        if($chk>0)
        {
            $data=Tax::where('tax_status','Active')
                     ->orderBy('id','DESC')
                     ->first();
            return $data;
        }
    }
    

    public function addtocart(Request $request)
    {
        if(isset($request->rec))
        {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->addRec($request->rec);
        }
        elseif(isset($request->snd_item_id))
        {
            $product = Product::find($request->item_id);
            $sndItm = ProductOneSubLevel::find($request->snd_item_id);
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->addSnd($product, $product->id,$sndItm,$sndItm->id);
        }
        elseif(isset($request->exec_menu))
        {
            parse_str($request->execArrayData, $searcharray);
            $execArrayData=$searcharray;
            $product = Product::find($request->item_id);
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->addexecMenu($product, $product->id,$execArrayData);
        }
        elseif(isset($request->pizza_menu))
        {
            $postcount=0;
            $nExtra=[];
            $searcharray=json_decode(json_encode(json_decode($request->cartData,true)),true);
            if(isset($searcharray))
            {
                if(count($searcharray)>0)
                {
                    $size=$searcharray['size_info'];;
                    $flabour=$searcharray['flabour'];
                    $extra=$searcharray['extra'];
                    if(count($extra)>0)
                    {
                        
                        foreach($extra as $ex):
                            if(!empty($ex))
                            {
                                $nExtra[]=array(
                                    'extra_name'=>$ex['extra_name'],
                                    'extra_id'=>$ex['extra_id'],
                                    'extra_price'=>$ex['extra_price'],
                                    'extra_quantity'=>$ex['extra_quantity']
                                );
                            }
                            
                        endforeach;
                    }
                }
            }
            //dd($nExtra);
            //$execArrayData=$searcharray;
            $product = Product::find($request->item_id);
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->addPizzaMenu($product, $product->id,$size,$flabour,$nExtra);
        }
        else
        {
            $product = Product::find($request->item_id);
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $product->id);
        }

        $request->session()->put('cart', $cart);
        return response()->json($cart);  
    }

    public function deltocart(Request $request)
    {
        //dd($request->lid);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->delProductFullRemove($request->lid);

        $request->session()->put('cart', $cart);
        return response()->json($cart); 
    }

    public function typeofdelivery(Request $request)
    {
        $cart = Session::has('cart') ? Session::get('cart') : null;
        return response()->json($cart->rec); 
    }

    public function cartJson()
    {
        $cart = Session::has('cart') ? Session::get('cart') : null;
        return response()->json($cart);  
    }

    public function ClearCart(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->ClearCart();

        $request->session()->put('cart', $cart);
        return response()->json($cart);  
    }

    public function categoryProduct($filter='')
    {
        $row=[];
        $category=category::where('product','!=',0)
                            ->get();
        foreach($category as $index=>$cat)
        {
            $row[$index]['id']=$cat->id;
            $row[$index]['name']=$cat->name;
            $row[$index]['layout']=$cat->layout;
            $row[$index]['product']=$cat->product;
            if($cat->layout==2)
            {
                $subCatData=[];
                $checkSubcid=SubCategory::where('category_id',$cat->id)->count();
                if($checkSubcid > 0)
                {
                    
                    $SubcidData=SubCategory::where('category_id',$cat->id)->get();
                    foreach($SubcidData as $inx=>$sc)
                    {
                        $subCatData[$inx]['id']=$sc->id;
                        $subCatData[$inx]['name']=$sc->name;

                        $product_row=[];
                        $product=Product::where('scid',$sc->id)->get();
                        //dd($product);
                        foreach($product as $key=>$pro)
                        {
                            if($pro->product_level_type==1)
                            {
                                $product_row[$key]['id']=$pro->id;
                                $product_row[$key]['name']=$pro->name;
                                $product_row[$key]['price']=$pro->price;
                                $product_row[$key]['interface']=$pro->product_level_type;
                                $product_row[$key]['description']=$pro->description;
                            }
                            elseif($pro->product_level_type==2)
                            {

                                $suboneData=[];
                                $product_row[$key]['id']=$pro->id;
                                $product_row[$key]['name']=$pro->name;
                                $product_row[$key]['price']=$pro->price;
                                $product_row[$key]['interface']=$pro->product_level_type;
                                $product_row[$key]['description']=$pro->description;
                                $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                                foreach($subOne as $soIndex=>$so)
                                {
                                    $suboneData[$soIndex]['id']=$so->id;
                                    $suboneData[$soIndex]['name']=$so->name;
                                    $suboneData[$soIndex]['price']=$so->price;
                                }
                                $product_row[$key]['ProductOneSubLevel']=$suboneData;
                            }
                            elseif($pro->product_level_type==3)
                            {

                                $suboneDatamodal=[];
                                $product_row[$key]['id']=$pro->id;
                                $product_row[$key]['name']=$pro->name;
                                $product_row[$key]['price']=$pro->price;
                                $product_row[$key]['interface']=$pro->product_level_type;
                                $product_row[$key]['description']=$pro->description;
                                $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();

                                foreach($subOne as $soIndex=>$so)
                                {
                                    $suboneDatamodal[$soIndex]['id']=$so->id;
                                    $suboneDatamodal[$soIndex]['name']=$so->name;
                                    $suboneDatamodal[$soIndex]['price']=$so->price;
                                }

                                $product_row[$key]['modal']=$suboneDatamodal;
                            }
                            elseif($pro->product_level_type==4)
                            {
                                $suboneData=[];
                                $product_row[$key]['id']=$pro->id;
                                $product_row[$key]['name']=$pro->name;
                                $product_row[$key]['price']=$pro->price;
                                $product_row[$key]['interface']=$pro->product_level_type;
                                $product_row[$key]['description']=$pro->description;
                                $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                                foreach($subOne as $soIndex=>$so)
                                {
                                    $suboneData[$soIndex]['id']=$so->id;
                                    $suboneData[$soIndex]['name']=$so->name;
                                    $dpsOP=explode(',', $so->description);
                                    $suboneData[$soIndex]['dropdown']=$dpsOP;


                                }

                                $product_row[$key]['ProductOneSubLevel']=$suboneData;
                            }
                            elseif($pro->product_level_type==5)
                            {
                                $suboneData=[];
                                $product_row[$key]['id']=$pro->id;
                                $product_row[$key]['name']=$pro->name;
                                $product_row[$key]['price']=$pro->price;
                                $product_row[$key]['interface']=$pro->product_level_type;
                                $product_row[$key]['description']=$pro->description;
                                
                                $pizzaSize=[];
                                $pizzaSql=PizzaSize::where('pid',$pro->id)->get();
                                foreach($pizzaSql as $SizeIndex=>$sz)
                                {
                                    $pizzaSize[$SizeIndex]['id']=$sz->id;
                                    $pizzaSize[$SizeIndex]['name']=$sz->name;
                                    $pizzaSize[$SizeIndex]['price']=$sz->price;
                                }
                                $product_row[$key]['PizzaSize']=$pizzaSize;

                                $PiFlabour=[];
                                $pizzaSql=PizzaFlabour::where('pid',$pro->id)->get();
                                foreach($pizzaSql as $plIndex=>$sl)
                                {
                                    $PiFlabour[$plIndex]['id']=$sl->id;
                                    $PiFlabour[$plIndex]['name']=$sl->name;
                                    $PiFlabour[$plIndex]['price']=$sl->price;
                                }
                                $product_row[$key]['PizzaFlabour']=$PiFlabour;


                                $suboneData=[];
                                $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                                foreach($subOne as $soIndex=>$so)
                                {
                                    $suboneData[$soIndex]['id']=$so->id;
                                    $suboneData[$soIndex]['name']=$so->name;
                                    $suboneData[$soIndex]['price']=$so->price;
                                }

                                $product_row[$key]['pizzaExtra']=$suboneData;
                            }
                        }

                        //dd($product_row);

                        $subCatData[$inx]['sub_product_row']=$product_row;
                    }
                       
                }

                $row[$index]['product_row']=$subCatData; 
                
            }
            else
            {
                $product_row=[];
                $product=Product::where('cid',$cat->id)->get();

                foreach($product as $key=>$pro)
                {
                   

                    if($pro->product_level_type==1)
                    {
                        $product_row[$key]['id']=$pro->id;
                        $product_row[$key]['name']=$pro->name;
                        $product_row[$key]['price']=$pro->price;
                        $product_row[$key]['interface']=$pro->product_level_type;
                        $product_row[$key]['description']=$pro->description;
                    }
                    elseif($pro->product_level_type==2)
                    {

                        $suboneData=[];
                        $product_row[$key]['id']=$pro->id;
                        $product_row[$key]['name']=$pro->name;
                        $product_row[$key]['price']=$pro->price;
                        $product_row[$key]['interface']=$pro->product_level_type;
                        $product_row[$key]['description']=$pro->description;
                        $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                        foreach($subOne as $soIndex=>$so)
                        {
                            $suboneData[$soIndex]['id']=$so->id;
                            $suboneData[$soIndex]['name']=$so->name;
                            $suboneData[$soIndex]['price']=$so->price;
                        }
                        $product_row[$key]['ProductOneSubLevel']=$suboneData;
                    }
                    elseif($pro->product_level_type==3)
                    {

                        $suboneDatamodal=[];
                        $product_row[$key]['id']=$pro->id;
                        $product_row[$key]['name']=$pro->name;
                        $product_row[$key]['price']=$pro->price;
                        $product_row[$key]['interface']=$pro->product_level_type;
                        $product_row[$key]['description']=$pro->description;
                        $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();

                        foreach($subOne as $soIndex=>$so)
                        {
                            $suboneDatamodal[$soIndex]['id']=$so->id;
                            $suboneDatamodal[$soIndex]['name']=$so->name;
                            $suboneDatamodal[$soIndex]['price']=$so->price;
                        }

                        $product_row[$key]['modal']=$suboneDatamodal;
                    }
                    elseif($pro->product_level_type==4)
                    {
                        $suboneData=[];
                        $product_row[$key]['id']=$pro->id;
                        $product_row[$key]['name']=$pro->name;
                        $product_row[$key]['price']=$pro->price;
                        $product_row[$key]['interface']=$pro->product_level_type;
                        $product_row[$key]['description']=$pro->description;
                        $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                        foreach($subOne as $soIndex=>$so)
                        {
                            $suboneData[$soIndex]['id']=$so->id;
                            $suboneData[$soIndex]['name']=$so->name;
                            $dpsOP=explode(',', $so->description);
                            $suboneData[$soIndex]['dropdown']=$dpsOP;


                        }

                        $product_row[$key]['ProductOneSubLevel']=$suboneData;
                    }
                    elseif($pro->product_level_type==5)
                    {
                        $suboneData=[];
                        $product_row[$key]['id']=$pro->id;
                        $product_row[$key]['name']=$pro->name;
                        $product_row[$key]['price']=$pro->price;
                        $product_row[$key]['interface']=$pro->product_level_type;
                        $product_row[$key]['description']=$pro->description;
                        
                        $pizzaSize=[];
                        $pizzaSql=PizzaSize::where('pid',$pro->id)->get();
                        foreach($pizzaSql as $SizeIndex=>$sz)
                        {
                            $pizzaSize[$SizeIndex]['id']=$sz->id;
                            $pizzaSize[$SizeIndex]['name']=$sz->name;
                            $pizzaSize[$SizeIndex]['price']=$sz->price;
                        }
                        $product_row[$key]['PizzaSize']=$pizzaSize;

                        $PiFlabour=[];
                        $pizzaSql=PizzaFlabour::where('pid',$pro->id)->get();
                        foreach($pizzaSql as $plIndex=>$sl)
                        {
                            $PiFlabour[$plIndex]['id']=$sl->id;
                            $PiFlabour[$plIndex]['name']=$sl->name;
                            $PiFlabour[$plIndex]['price']=$sl->price;
                        }
                        $product_row[$key]['PizzaFlabour']=$PiFlabour;


                        $suboneData=[];
                        $subOne=ProductOneSubLevel::where('pid',$pro->id)->get();
                        foreach($subOne as $soIndex=>$so)
                        {
                            $suboneData[$soIndex]['id']=$so->id;
                            $suboneData[$soIndex]['name']=$so->name;
                            $suboneData[$soIndex]['price']=$so->price;
                        }

                        $product_row[$key]['pizzaExtra']=$suboneData;


                    }
                }

                $row[$index]['product_row']=$product_row;
            }

        }

        return $row;
    }

    public function product()
    {
        $product=$this->categoryProduct(
        );
        return response()->json($product);
    }

    public function getPayment()
    {
        return view('frontend.pages.checkout.select-payment');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductItem  $productItem
     * @return \Illuminate\Http\Response
     */
    public function show(ProductItem $productItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductItem  $productItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductItem $productItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductItem  $productItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductItem $productItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductItem  $productItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductItem $productItem)
    {
        //
    }
}
