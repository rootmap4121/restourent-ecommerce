<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Facade;
use App\Product;
use Session;
use App\Cart;

use Illuminate\Http\Request;
use App\Seo;
use App\QRCode;
use App\category;
use App\SubCategory;
use App\Brand;
use App\Slider;
use App\CustomerSupport;
use App\ContactDetail;
use App\Language;
use App\Currency;
use App\ProductReview;
use App\Shipping;
use App\Orders;
use App\OrdersItem;
use App\DeliveryAddress;
use App\PaymentMethod;
use App\ProductViewByIP;
//MenuPageController::loggedUser('company_prefix')
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class MenuPageController extends Facade {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $SiteName="The Bombay Caottage";
    public $SiteURL="thebombaycottage.com";
    public $TableReservationAdminEmail="fakhrulislamtalukder@gmail.com";
    public $ContactAdminEmail="fakhrulislamtalukder@gmail.com";
    public function __construct(){ 
        $dataSit=CustomerSupport::find(1);
        $this->TableReservationAdminEmail=$dataSit->reservation_email;
        $this->ContactAdminEmail=$dataSit->contact_admin_email;
    }

    protected static function getFacadeAccessor() {
        //what you want
        return $this;
    }

    
    
    
    public static function TableUserOrder($name='')
    {
        return "Dear ".$name.", Thank you for shopping at The Bombay Caottage. We hope you will enjoy your new purchase! We have included your order receipt and delivery details below:";
    }

    public static function TableReservation($name='')
    {
        return "Dear ".$name.", Thank you for table reservation at The Bombay Caottage. We hope you will enjoy our restourent! We have included your Reservation info and datetime details below:";
    }

    
    public static function ContactQueryHead($name='')
    {
        return "Dear ".$name.", Thank you for contact query at The Bombay Caottage. We will get back to you as soon as possible! We have included your contact request info and datetime details below:";
    }
    

    public static function mainMenu()
    {
        $cat=category::all();
        return $cat;
    }

    public static function CustomerSupport()
    {
        $cs = CustomerSupport::orderBy('id','DESC')->first();
        return $cs;
    }

    public static function ContactDetail()
    {
        $cs = ContactDetail::select('*',DB::Raw('(SELECT qrcode FROM q_r_codes ORDER BY id DESC LIMIT 1) as qr_code'))->orderBy('id','DESC')->first();
        //dd($cs);
        return $cs;
    }

    public static function Seo()
    {
        $cs = Seo::orderBy('id','DESC')->first();
        //dd($cs);
        return $cs;
    }

    public static function SliderData()
    {
        $cs = Slider::where('isactive',1)->orderBy('id','DESC')->get();
        //dd($cs);
        return $cs;
    }

    public static function recentProduct() {
        $countProduct = Product::count();
        if ($countProduct != 0) {
            $sqlProduct = Product::where('isactive', '0')->where('parent_product',0)->orderBy('id','DESC')->take(2)->get();
            return $sqlProduct;
        }
    }
    
    public static function recentProductView($pid=0) {
        $tab=new ProductViewByIP();
        $tab->product_id=$pid;
        $tab->ip=$_SERVER["REMOTE_ADDR"];
        $tab->save();
    }
    
    public static function recentProductViewShow() {
        $countProduct = ProductViewByIP::where('ip',$_SERVER["REMOTE_ADDR"])->count();
        if ($countProduct != 0) {
            $sqlProduct =DB::table('product_view_by_i_ps')
                             ->where('product_view_by_i_ps.ip',$_SERVER["REMOTE_ADDR"])
                             ->join('products','product_view_by_i_ps.product_id','=','products.id')
                             ->select(DB::Raw('products.*'))
                             ->orderBy('product_view_by_i_ps.id','DESC')
                             ->groupBy('product_view_by_i_ps.product_id')
                             ->take(2)
                             ->get();
            
            
            ///$sqlProduct = Product::where('isactive', '0')->where('parent_product',0)->orderBy('id','DESC')->take(2)->get();
            $retArr=array('total'=>count($sqlProduct),'data'=>$sqlProduct);
            return $retArr;
        }
    }

    public static function CurrencyDetail() {
        $sqlProduct =Currency::where('isactive',1)->first();
        return $sqlProduct;
    }
    
    public static function QRCode() {
        $countProduct = QRCode::count();
        if ($countProduct != 0) {
            $sqlProduct = QRCode::find(1);
            return $sqlProduct;
        }
    }

    public static function shoppingCart() {
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        
        $array=[
            'products' => $cart->items,
            'totalQty' => $cart->totalQty,
            'totalPrice' => $cart->totalPrice
        ];
        
        if (isset($array)) {
            return $array;
        }
    }
    
    
    public static function siteBasic()
    {
            $language = Language::all();
            $currency = Currency::all();

            $cs = CustomerSupport::all();
            $cde = ContactDetail::all();

            //$subcat=SubCategory::all();
            $cat = category::take(2)->get();

            $cats = category::all();
            $brn = Brand::all();
            $seo = Seo::all();
        
        
            $arr=
            [
            'language' => $language,
            'currency' => $currency,
            'cussup' => $cs,
            'csudet' => $cde,
            'seo' => $seo,
            'cat' => $cat,
            'brn' => $brn,
            'cats' => $cats
            ];
            
            return $arr;
    }
    
    public static function showSubCategory($cid=0)
    {
        $tab=SubCategory::where('category_id',$cid)->get();
        return $tab;
    }
    
    
    public function initMail(
        $to='',
        $subject='',
        $body='',
        $bcc='',
        $AltBody='This is the body in plain text for non-HTML mail clients',
        $attachment='',
        $debug=0
    )
    {
          $mail = new PHPMailer(true);
          try {
              $mail->SMTPDebug = $debug;
              $mail->isSMTP(); 
              $mail->Host = 'mail.spxce.co';
              $mail->SMTPAuth = true;
              $mail->Username = 'simpleretailpos@spxce.co';
              $mail->Password = '@sdQwe123';
              $mail->SMTPSecure = 'tls';
              $mail->Port = 587;

              $mail->setFrom('info@spxce.co', $this->SiteName);

              //$mail->addAddress($to, 'Fahad Bhuyian');
              $mail->addAddress($to);               // Name is optional
              $mail->addReplyTo('info@'.$this->SiteURL, 'Reply - '.$this->SiteName);
             // $mail->addCC('cc@example.com');
              if(!empty($bcc))
              {
                 $mail->addBCC($bcc);
              }
              

              //Attachments
              if(!empty($attachment))
              {
                 $mail->addAttachment($attachment);
              }
              //$mail->addAttachment('/var/tmp/file.tar.gz');
              //$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); 

              //Content
              $mail->isHTML(true);
              $mail->Subject = $subject;
              $mail->Body    = $body;
              $mail->AltBody = $AltBody;
              $mail->send();
              return 1;
          } catch (Exception $e) {
              if($debug>0)
              {
                  return 'Message could not be sent. Mailer Error: '.$mail->ErrorInfo;
              }
              else
              {
                  return 0;
              }
          }
    }
    

}
