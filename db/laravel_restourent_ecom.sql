-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2018 at 09:20 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_restourent_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Saiful Islam', 'saiful@gmail.com', '$2y$10$PA5Qt1EGO0VMpi1lrvCOWeHMvFixe/mWeVlw.nSHVH7vdDqj.Urqy', NULL, '2017-04-10 13:41:13', '2017-04-10 13:41:13');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brandimage` text COLLATE utf8_unicode_ci NOT NULL,
  `brandlogo` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` text COLLATE utf8_unicode_ci NOT NULL,
  `is_custom_layout` tinyint(1) NOT NULL,
  `layout` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `brandimage`, `brandlogo`, `description`, `isactive`, `is_custom_layout`, `layout`, `created_at`, `updated_at`) VALUES
(1, 'Portland Academy', '1506026439.jpg', '1506026439.png', 'The ultimate combination of elegance, strength and durability; the superior Porland Academy fine china collection features warm, white refined shapes to enhance every dining occasion. All items in the collection conform to BS4034 and are microwave, oven, dishwasher and freezer safe.', 'on', 1, 1, '2017-02-15 11:21:43', '2018-01-04 15:03:43'),
(7, 'Porcelite', '1506026138.jpg', '1506026138.png', 'A powerful blend of quality, performance and value, the renowned Porcelite collection of professional vitrified porcelain offers a versatile and cost-effective solution without compromise.', 'on', 0, 0, '2017-09-17 20:47:00', '2017-09-22 01:35:38'),
(8, 'Seasons by Porcelite', '1506026515.jpg', '1506026515.png', 'Inspired presentation with the rustic look of Seasons by Porcelite. This award winning, hand-decorated vitrified porcelain collection features eight complementary colours to add real rustic charm and individuality to casual and fine dining menus.', 'on', 0, 0, '2017-09-17 21:36:49', '2017-09-22 01:41:55'),
(9, 'Porland Studio Perspective', '1509118135.jpg', '1506028389.jpg', 'Designed by the world renowned Studio Levien of London, Perspective features a clever off-set design offering the chef unlimited presentation options.\r\nCrafted in the same hard paste porcelain as Porcelite, Perspective presents well alongside Porcelite offering a differentiated look. All items in the collection are microwave and dishwasher safe.', 'on', 0, 0, '2017-09-17 21:41:52', '2017-10-27 20:28:55'),
(11, 'Signatures', '1506028534.jpg', '1506028534.jpg', 'A contemporary porcelain collection designed for the modern chef. A unique range providing endless possibilities for innovative food presentation', 'on', 0, 0, '2017-09-17 21:47:07', '2017-09-22 02:15:34'),
(12, 'Connoisseur Fine Bone China', '1506028836.jpg', '1506028836.png', 'A beautiful collection of brilliant white fine bone china with classic shapes to suit fine dining menus. The ultimate, affordable fine bone china option for all establishments', '', 0, 0, '2017-09-17 21:48:59', '2017-10-28 14:39:18'),
(16, 'Australian Fine China', '1506029019.jpg', '1506029019.png', 'The ultimate combination of elegance, strength and durability; the superior Porland Academy fine china collection features warm, white refined shapes to enhance every dining occasion.All items in the collection conform to BS4034 and are microwave, oven, dishwasher and freezer safe.', 'on', 0, 0, '2017-09-22 01:05:14', '2017-09-22 02:23:39'),
(17, 'Costa Verde', '1509118354.jpg', '1509118354.png', 'Inspired by the world’s finest cuisines, the Costa Verde professional porcelain collections offer contemporary, European styling combined with exceptional quality and durability.\r\nAll items are microwave, dishwasher, oven & freezer safe.', 'on', 0, 0, '2017-10-27 20:32:34', '2017-10-27 20:32:34'),
(18, 'Vista Alegre', '1509651051.jpg', '1509118473.jpg', 'A contemporary porcelain collection designed for the modern chef. A unique range providing endless possibilities for innovative food presentation', 'on', 0, 0, '2017-10-27 20:34:33', '2017-11-03 00:30:51'),
(21, 'Rustico Stoneware', '1509118662.jpg', '1509118662.png', 'Add rustic charm with Rustico Stoneware. The perfect platform for truly distinguished presentation, this unique Stoneware collection features versatile items alongside signature pieces to work across the menu. Reactive glazes offer endless combinations, adding real wow factor to casual and fine dining dishes. Microwave and dishwasher safe.', 'on', 0, 0, '2017-10-27 20:37:42', '2017-10-27 20:37:42'),
(22, 'Bevande', '1509118762.jpg', '1509118762.png', 'Classic design with a contemporary finish, this durable porcelain collection features five fresh, complementary colours designed to add interest to modern beverage service. Mix and match to create striking combinations. Microwave & dishwasher safe.', 'on', 0, 0, '2017-10-27 20:39:22', '2017-10-31 23:33:06'),
(23, 'Simply', '1509118839.jpg', '1509118839.png', 'The Simply collection offers outstanding quality and value with multi-functional items designed to work across the menu.', 'on', 0, 0, '2017-10-27 20:40:39', '2017-10-27 20:40:39'),
(24, 'Elegance 18/10', '1509739319.jpg', '1509739319.jpg', 'Elegance', 'on', 0, 0, '2017-11-04 01:01:59', '2017-11-04 01:01:59'),
(25, 'Portland Academy Line', '1509744028.jpg', '1509743759.jpg', 'Portland Academy line', 'on', 0, 0, '2017-11-04 02:05:15', '2017-11-04 02:20:28'),
(26, 'Glass Bottles', '1510862826.jpg', '1510862826.png', 'Glass bottles', 'on', 0, 0, '2017-11-17 02:07:06', '2017-11-17 02:07:06'),
(27, 'Stolzle', '1511293353.jpg', '1511293353.png', 'Stolzle', 'on', 0, 0, '2017-11-22 01:42:33', '2017-11-22 01:42:33'),
(28, 'Brand Custom-S', '1516733496.jpg', '1516733496.jpg', 'dsfsfdsffs', 'on', 1, 2, '2018-01-01 13:08:56', '2018-01-23 12:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `layout` int(1) NOT NULL DEFAULT '1' COMMENT 'Layout Type -> cat -> scat',
  `product` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='All Main Categories';

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `layout`, `product`, `created_at`, `updated_at`) VALUES
(1, 'STARTERS', NULL, 1, 4, '2018-04-19 12:40:16', '2018-04-22 12:32:06'),
(2, 'Executive Set Meal', 'Meal For 2 Persons\r\n\r\nWorth £34.95 as per menu\r\n\r\nAppetiser : 2 popadoms | Chutney Set\r\n\r\n2 Starter : Chicken Pakura | Vegetable Pakora\r\n\r\n2 Main Dishes : With Chicken /  Vegetable /  Lamb or Prawn\r\nKurma* (Mild), Bhuna (Medium), Rogan Josh (Medium), Pathia (Medium to Hot), Madras (Hot), Jalfrezi (Fairly Hot)\r\n\r\nTwo Sundries: Pilau Rice, Plain Nan', 1, 3, '2018-04-19 12:42:38', '2018-04-22 10:50:24'),
(3, 'Pizza', NULL, 1, 4, '2018-04-19 12:46:05', '2018-04-23 13:41:48'),
(4, 'Traditional Set Menu', NULL, 2, 3, '2018-04-19 12:56:31', '2018-04-19 13:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `color_in_products`
--

CREATE TABLE `color_in_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_details`
--

CREATE TABLE `contact_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_terms_condition` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_details`
--

INSERT INTO `contact_details` (`id`, `contact_title`, `contact_address`, `contact_phone`, `contact_email`, `site_terms_condition`, `created_at`, `updated_at`) VALUES
(1, 'Support Contact', 'Dhaka, Bangladesh', '01860748020', 'kamal@ecommerce.com', 'Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here \r\n\r\nsome text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some \r\n\r\ntext Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text Here some text .', '2017-03-25 14:42:42', '2017-08-07 14:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `contact_pages`
--

CREATE TABLE `contact_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact_pages`
--

INSERT INTO `contact_pages` (`id`, `heading`, `name`, `description`, `isactive`, `created_at`, `updated_at`) VALUES
(2, 'Delivery', 'Delivery', 'Delivery content here', 1, '2017-07-08 13:20:59', '2017-07-08 13:20:59'),
(3, 'Secure Payment', 'Secure-payment', 'Secure Payment here', 1, '2017-07-08 13:21:32', '2017-07-08 13:21:32'),
(4, 'Legal Notice', 'Legal-Notice', 'Legal Notice will appear here', 1, '2017-07-08 13:21:57', '2017-07-08 13:21:57'),
(5, 'Terms of use', 'Terms-of-use', 'Terms of use content will show here', 1, '2017-07-08 13:22:26', '2017-07-08 13:22:26'),
(6, 'About us', 'About-us', 'About Text will show here.', 1, '2017-07-29 10:26:28', '2017-07-29 10:26:28');

-- --------------------------------------------------------

--
-- Table structure for table `contact_requests`
--

CREATE TABLE `contact_requests` (
  `id` int(20) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `description` longtext,
  `contact_status` enum('Active','Pending') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_requests`
--

INSERT INTO `contact_requests` (`id`, `name`, `email`, `phone`, `description`, `contact_status`, `created_at`, `updated_at`) VALUES
(1, 'Fahad', 'f.bhuyian@gmail.com', '01860748020', 'gggggg', NULL, '2017-07-27 20:53:28', '0000-00-00 00:00:00'),
(2, 'Fahad', 'f.bhuyian@gmail.com', '01860748020', 'ggggggg vggg', NULL, '2017-07-27 20:58:56', '0000-00-00 00:00:00'),
(3, 'sdfdsf', 'sdfsdf', 'dsfsdf', 'dsfsdfdsf', NULL, '2017-07-27 21:24:17', '0000-00-00 00:00:00'),
(4, 'dfgdfgdfg', 'dfgdfgdfg', 'dfgdfgdf', 'dfgdfg', NULL, '2017-07-27 21:25:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` text COLLATE utf8_unicode_ci NOT NULL,
  `short_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `icon`, `short_code`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'GBP', '£', 'GBP', 1, '2017-03-25 16:13:05', '2018-01-05 11:51:20'),
(3, 'Dollar (USD)', '$', 'USD', 0, '2017-03-25 16:15:30', '2017-03-25 16:24:25'),
(4, 'Euro (EUR)', '€', 'EUR', 0, '2017-03-25 16:15:53', '2017-03-25 16:24:36'),
(5, 'BDT (TAKA)', '৳', 'BDT', 0, '2017-03-25 16:25:18', '2017-03-25 16:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('Customer','Admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `sex`, `dob`, `phone`, `email`, `password`, `user_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Fahad Bhuyian', '', '0000-00-00', '01860748020', 'f.bhuyian@gmail.com', '$2y$10$RQ1vGqVcfvw8xbGu7Hm1/erdY5KxSXXvJOalCQYWqE1KoTb4s7nkC', 'Admin', NULL, '2017-04-10 14:03:25', '2017-07-24 13:51:00'),
(2, 'Fahad', 'Male', '0000-00-00', '', 'fahad@systechunimax.com', '$2y$10$34pUIoqLpROoWjm9omAZhe77Ko9lGUI5ra3mcuuHEJllc3pigQbD.', 'Customer', NULL, '2017-04-15 13:58:20', '2017-04-15 13:58:20'),
(3, 'Fahad Bhuyian', 'Male', '0000-00-00', '', 'fahadvampare@gmail.com', '$2y$10$.88YPT8NZklZKKmZU2gWLOLMha1GkpXRyeuVQtLoWxSRV5jwazfDG', 'Customer', NULL, '2017-05-02 12:35:00', '2017-05-02 12:35:00'),
(4, 'Fahad', 'Male', '0000-00-00', '', 'fahad@gmail.com', '$2y$10$Ou65eimeIv2UZ4DVuGVP5eECS48KLYufkipBd521TlwiJtGChVgre', 'Customer', NULL, '2017-06-02 13:06:35', '2017-06-02 13:06:35'),
(5, 'rony', 'Male', '0000-00-00', '', 'rony@gmail.com', '$2y$10$QozSdc2WW3T.WUj8ug8JQeCLWuRgJIZYZPXMqNCxBosJUqnF5wBue', 'Customer', NULL, '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(7, 'Fakhrul', 'Male', '0000-00-00', '', 'fakhrul606399@gmail.com', '$2y$10$CAMYBzbXOs7nyZiZuU9/ceZ1y4wct26L1b20PqBCf8lJEISwm8W8m', 'Customer', NULL, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(8, 'shaiful Islam', 'Male', '0000-00-00', '', 'shaiful1408@gmail.com', '$2y$10$iSVqWUTHanoBcXqpD1DLHujyI09bVQI/GCbW2FkYZTxs62VJH73GW', 'Admin', NULL, '2017-07-15 13:27:07', '2017-07-24 13:39:14'),
(9, 'Parash', 'Male', '0000-00-00', '', 'fakhrul_islam21@diit.info', '$2y$10$mw/bEUQrmVcVUK/le4f2buKJuvSn6Uovcx3oX0im8GRHwyS16ow46', 'Customer', NULL, '2017-07-24 05:20:21', '2017-07-24 05:20:21'),
(10, 'Kamal hemel', 'Male', '0000-00-00', '', 'kamalhemel@gmail.com', '$2y$10$yEDEzpgrNZbG/558V5F/Be1rsYwho2HUJvblbjY1jY0Z9j/Oa970W', 'Customer', NULL, '2017-09-19 19:58:29', '2017-09-19 19:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `customer_supports`
--

CREATE TABLE `customer_supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_supports`
--

INSERT INTO `customer_supports` (`id`, `contact_number`, `created_at`, `updated_at`) VALUES
(1, '01860748020', '2017-03-25 14:22:34', '2017-03-25 14:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` int(10) UNSIGNED NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `additional_info` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `customer_id`, `token`, `first_name`, `last_name`, `company`, `address`, `address_2`, `city`, `state`, `zip_code`, `country`, `additional_info`, `home_phone`, `mobile_phone`, `created_at`, `updated_at`) VALUES
(1, 1, '', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'sdsadsad', 'sadsadsa', 'Dhaka', 2, '12345', 21, 'sadsadsad', '01860748020', '01860748020', '2017-04-10 14:03:25', '2017-04-10 14:03:25'),
(11, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfsdf', 'sdfsdf', 'sdfds', 'fdsfds', 'fsdfsdfsd', 'fdsfsd', 2, '12345', 21, 'dfdsf', '01860748020', '01860748020', '2017-04-15 13:58:20', '2017-04-15 13:58:20'),
(12, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:21:43', '2017-04-15 14:21:43'),
(13, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:22:53', '2017-04-15 14:22:53'),
(14, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:23:23', '2017-04-15 14:23:23'),
(15, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:33:10', '2017-04-15 14:33:10'),
(16, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:36:43', '2017-04-15 14:36:43'),
(17, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:37:10', '2017-04-15 14:37:10'),
(18, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:37:46', '2017-04-15 14:37:46'),
(19, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:38:06', '2017-04-15 14:38:06'),
(20, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:38:25', '2017-04-15 14:38:25'),
(21, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:38:51', '2017-04-15 14:38:51'),
(22, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:39:10', '2017-04-15 14:39:10'),
(23, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:40:14', '2017-04-15 14:40:14'),
(24, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:42:25', '2017-04-15 14:42:25'),
(25, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'ccvvc', 'cvvcc', 'cvcvvc', 'cvcvcv', 'cvcv', 'dhaka', 6, '12345', 21, 'ssddssd', '01860748020', '01860748020', '2017-04-15 14:51:47', '2017-04-15 14:51:47'),
(26, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'asdsad', 'asdas', 'das', 'dsadsa', 'dsadsa', 'dsadsa', 3, '12345', 21, 'dfsfdsf', '01860748020', '01860748020', '2017-04-15 15:14:34', '2017-04-15 15:14:34'),
(27, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:16:38', '2017-04-15 15:16:38'),
(28, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:17:07', '2017-04-15 15:17:07'),
(29, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:20:21', '2017-04-15 15:20:21'),
(30, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:23:56', '2017-04-15 15:23:56'),
(31, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:24:52', '2017-04-15 15:24:52'),
(32, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:25:03', '2017-04-15 15:25:03'),
(33, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:25:41', '2017-04-15 15:25:41'),
(34, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:27:20', '2017-04-15 15:27:20'),
(35, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:28:15', '2017-04-15 15:28:15'),
(36, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:28:35', '2017-04-15 15:28:35'),
(37, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:28:56', '2017-04-15 15:28:56'),
(38, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:30:31', '2017-04-15 15:30:31'),
(39, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:30:45', '2017-04-15 15:30:45'),
(40, 2, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sdfdsf', 'sdfds', 'fsdfds', 'fsdf', 'sdfsdf', 'sdfsdf', 3, '12345', 21, 'sdfdsfsd', '01860748020', '01860748020', '2017-04-15 15:31:19', '2017-04-15 15:31:19'),
(41, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:21:14', '2017-04-16 12:21:14'),
(42, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:24:43', '2017-04-16 12:24:43'),
(43, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:25:40', '2017-04-16 12:25:40'),
(44, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:26:14', '2017-04-16 12:26:14'),
(45, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:26:38', '2017-04-16 12:26:38'),
(46, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:34:49', '2017-04-16 12:34:49'),
(47, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:36:50', '2017-04-16 12:36:50'),
(48, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:37:09', '2017-04-16 12:37:09'),
(49, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:37:46', '2017-04-16 12:37:46'),
(50, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:39:49', '2017-04-16 12:39:49'),
(51, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:40:19', '2017-04-16 12:40:19'),
(52, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:40:37', '2017-04-16 12:40:37'),
(53, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:41:53', '2017-04-16 12:41:53'),
(54, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:43:38', '2017-04-16 12:43:38'),
(55, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:44:46', '2017-04-16 12:44:46'),
(56, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:45:47', '2017-04-16 12:45:47'),
(57, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:46:38', '2017-04-16 12:46:38'),
(58, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:47:01', '2017-04-16 12:47:01'),
(59, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:47:37', '2017-04-16 12:47:37'),
(60, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:47:51', '2017-04-16 12:47:51'),
(61, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:48:07', '2017-04-16 12:48:07'),
(62, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:48:37', '2017-04-16 12:48:37'),
(63, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 12:51:14', '2017-04-16 12:51:14'),
(64, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 13:07:41', '2017-04-16 13:07:41'),
(65, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 13:08:11', '2017-04-16 13:08:11'),
(66, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 13:10:47', '2017-04-16 13:10:47'),
(67, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'Bhuyian', 'Systech Unimax Ltd', 'None', 'sdfdsf', 'Dhaka', 4, '12045', 21, 'xzcxzcxzc', '01860748020', '01860748020', '2017-04-16 13:20:52', '2017-04-16 13:20:52'),
(68, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'Fahad', 'bhuyian', 'Systech unimax ltd', 'sdfdsf', 'asdsadsa', 'dhaka', 2, '12345', 21, 'sdfsdfdsf', '01860748020', '01860748020', '2017-04-16 13:22:27', '2017-04-16 13:22:27'),
(69, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'dsfsdf', 'dhaka', 2, '12546', 21, 'sdsadsadsa', '01860748020', '01860748020', '2017-04-16 13:24:02', '2017-04-16 13:24:02'),
(70, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'dgdfgfdg', 'dfgdf', 'gdfgfdg', 'fdgfd', 'dfg', 'dfgdf', 3, '11234', 21, 'dfgfdg', '01860748020', '01860748020', '2017-04-16 13:24:58', '2017-04-16 13:24:58'),
(71, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'sdfdsf', 'sdfsdf', 'sdfdsfds', 'fdsfdsf', 'dsfsdf', 'sdfsdfds', 3, '12345', 21, 'sdfdsfdsfd', '01860748020', '01860748020', '2017-04-16 13:43:10', '2017-04-16 13:43:10'),
(72, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'sdfsdf', 'sdfdsfsd', 'fsdfsd', 'fsdf', 'sfsdf', 'sdfdsf', 3, '12345', 21, 'sdfdsfdsfds', '01860748020', '01860748020', '2017-04-16 13:51:05', '2017-04-16 13:51:05'),
(73, 2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 'sdfdsf', 'dsfdsfd', 'sfdsfsdf', 'dsfdsf', 'dsfdsf', 'dsfdsfds', 2, '12345', 21, 'sdfdsfsdf', '01860748020', '01860748020', '2017-04-16 14:21:18', '2017-04-16 14:21:18'),
(74, 2, 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 'dds', 'ssd', 'sds', 's', 'ss', 'ss', 2, '12345', 21, 'sddsds', '01860748020', '01860748020', '2017-04-17 11:36:51', '2017-04-17 11:36:51'),
(75, 2, 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 'sdfdsf', 'sdfds', 'fdsf', 'sdfsd', 'fsd', 'sdfsdfd', 4, '12093', 21, 'sadsad', '01860748020', '01860748020', '2017-05-01 12:22:36', '2017-05-01 12:22:36'),
(76, 3, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 'asdsad', 'sadasd', 'sadsa', 'dasd', 'sadsad', 'asdsa', 8, '12345', 21, 'asdsadasd', '01860748020', '01860748020', '2017-05-02 12:35:01', '2017-05-02 12:35:01'),
(77, 2, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 'asdadsa', 'dadas', 'dadas', 'dasdas', 'dasdasdas', 'dasdasd', 3, '12345', 21, 'asdsadsad', '01860748020', '01860748020', '2017-05-02 13:14:33', '2017-05-02 13:14:33'),
(78, 2, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 'sdfsdf', 'dsfdsf', 'dsfds', 'dsfsdf', 'sdfsd', 'dsfdsf', 5, '12312', 21, 'ewqewqewq', '213323211', '232232323221', '2017-05-02 13:37:53', '2017-05-02 13:37:53'),
(79, 2, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 'cxzczxczx', 'xzczxc', 'xcxzczx', 'zxczxczx', 'xzczxczx', 'cxxzcxzcxz', 13, '21121', 21, 'asdsadasd', '11111111111111', '1232243543', '2017-05-02 13:42:40', '2017-05-02 13:42:40'),
(80, 4, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-06-03 11:18:39', '2017-06-03 11:18:39'),
(81, 4, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 'Md Mahamod', 'Not Mention', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(82, 4, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 'Md Mahamudur', 'Zaman', 'Not Mention', '3A, Shorno Kunzo, Ltd1', 'Not Mention', 'Dhaka', 0, '1209', 3, 'd', 'Not Mention', '01860748020', '2017-06-04 11:43:42', '2017-06-04 11:43:42'),
(83, 4, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 'Md Mahamudur Zaman', 'Bhuyian', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'sadas', 'Not Mention', '01860748020', '2017-06-05 12:11:26', '2017-06-05 12:11:26'),
(84, 5, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 'sdcsd', 'scdfscs', 'a', 'd', 'sdfsf', 'asdas', 0, '1111', 3, 'Not Mention', '32424234', '234234234', '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(85, 5, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 'sdcsd', 'scdfscs', 'a', 'd', 'sdfsf', 'asdas', 0, '1111', 3, 'Not Mention', '32424234', '234234234', '2017-06-07 13:46:54', '2017-06-07 13:46:54'),
(86, 5, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 'sdcsd', 'scdfscs', 'a', 'd', 'sdfsf', 'asdas', 0, '1111', 3, 'Not Mention', '32424234', '234234234', '2017-06-07 13:47:34', '2017-06-07 13:47:34'),
(87, 6, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-07 13:55:40', '2017-06-07 13:55:40'),
(88, 6, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-07 13:57:49', '2017-06-07 13:57:49'),
(89, 6, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-09 11:07:21', '2017-06-09 11:07:21'),
(90, 6, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-09 11:07:59', '2017-06-09 11:07:59'),
(91, 6, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-09 11:08:48', '2017-06-09 11:08:48'),
(92, 6, 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 'sdcsd', 'scdfscs', 'a', 'sdsf', 'sdfsf', 'asdasd', 0, '1111', 3, 'asxdsaxa', '32424234', '234234234', '2017-06-09 13:47:31', '2017-06-09 13:47:31'),
(93, 7, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(94, 7, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-13 01:16:45', '2017-07-13 01:16:45'),
(95, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:39', '2017-07-15 12:39:39'),
(96, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(97, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(98, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(99, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(100, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(101, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:41:43', '2017-07-15 12:41:43'),
(102, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:53:25', '2017-07-15 12:53:25'),
(103, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:56:18', '2017-07-15 12:56:18'),
(104, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 12:58:47', '2017-07-15 12:58:47'),
(105, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:01:13', '2017-07-15 13:01:13'),
(106, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:02:54', '2017-07-15 13:02:54'),
(107, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:04:07', '2017-07-15 13:04:07'),
(108, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(109, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(110, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(111, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(112, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(113, 7, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-15 13:17:01', '2017-07-15 13:17:01'),
(114, 8, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 'Demo', 'Demo', 'dadsada', 'sasda', 'dasasda', 'Dhaka', 0, '1215', 3, 'xaskcnkjncjksa', '123456789', '123456789', '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(115, 7, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-21 10:37:49', '2017-07-21 10:37:49'),
(116, 7, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-21 10:38:02', '2017-07-21 10:38:02'),
(117, 7, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-21 10:38:05', '2017-07-21 10:38:05'),
(118, 7, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-22 11:36:47', '2017-07-22 11:36:47'),
(119, 7, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-22 11:36:54', '2017-07-22 11:36:54'),
(120, 7, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', '01677136045', '01677136045', '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(121, 4, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-22 13:09:37', '2017-07-22 13:09:37'),
(122, 4, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-22 13:10:07', '2017-07-22 13:10:07'),
(123, 4, 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 08:59:01', '2017-07-23 08:59:01'),
(124, 4, '1500822546', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:09:06', '2017-07-23 09:09:06'),
(125, 4, '1500822573', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:09:33', '2017-07-23 09:09:33'),
(126, 4, '1500822656', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:10:56', '2017-07-23 09:10:56'),
(127, 4, '1500823608', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:26:48', '2017-07-23 09:26:48'),
(128, 4, '1500824005', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:33:25', '2017-07-23 09:33:25'),
(129, 4, '1500825424', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 09:57:04', '2017-07-23 09:57:04'),
(130, 4, '1500826369', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-07-23 10:12:49', '2017-07-23 10:12:49'),
(131, 7, '1500827051', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-23 10:24:11', '2017-07-23 10:24:11'),
(132, 7, '1500827110', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-23 10:25:10', '2017-07-23 10:25:10'),
(133, 7, '1500827405', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-23 10:30:15', '2017-07-23 10:30:15'),
(134, 9, '1500895160', 'fakkhrul', 'Islam', 'Not Mention', 'ltd 1 md.pur', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Not Mention', '01677136045', '01677136045', '2017-07-24 05:20:21', '2017-07-24 05:20:21'),
(135, 2, '1500927850', 'sdfsdf', 'sdfsdf', 'sdfds', 'fdsfds', 'fsdfsdfsd', 'fdsfsd', 2, '12345', 0, 'dfdsf', '01860748020', '01860748020', '2017-07-24 14:24:22', '2017-07-24 14:24:22'),
(136, 2, '1500927893', 'sdfsdf', 'sdfsdf', 'sdfds', 'fdsfds', 'fsdfsdfsd', 'fdsfsd', 2, '12345', 0, 'dfdsf', '01860748020', '01860748020', '2017-07-24 14:24:59', '2017-07-24 14:24:59'),
(137, 7, '1500928172', 'fakhrul', 'islam', 'skeletonit', 'mdpur ltd 1', 'Not Mention', 'Dhaka', 0, '1215', 3, 'Null', 'Not Mention', '01677136045', '2017-07-24 14:29:43', '2017-07-24 14:29:43'),
(138, 4, '1504863037', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(139, 4, '1504863865', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(140, 4, '1504892353', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-09-08 11:39:19', '2017-09-08 11:39:19'),
(141, 4, '1504893834', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2017-09-08 12:04:14', '2017-09-08 12:04:14'),
(142, 8, '1505156641', 'Demo', 'Demo', 'dadsada', 'sasda', 'dasasda', 'Dhaka', 0, '1215', 3, 'xaskcnkjncjksa', '123456789', '123456789', '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(143, 10, '1505832923', 'kamal', 'hemel', 'Not Mention', '66 Indira road', 'Not Mention', 'Dhaka', 0, '1408', 3, 'test', 'Not Mention', '01781709777', '2017-09-19 19:58:29', '2017-09-19 19:58:29'),
(144, 4, '1516822127', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(145, 4, '1516990277', 'Md Mahamod', 'Zaman', 'Not Mention', '3A, Shornokunzo, LTD1, Mohammodpur', 'Not Mention', 'Dhaka', 0, '1209', 3, 'Not Mention', 'Not Mention', '01860748020', '2018-01-26 12:14:35', '2018-01-26 12:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_addresses`
--

CREATE TABLE `invoice_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` int(11) NOT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `additional_info` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `languageimage` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `languageimage`, `description`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'English', '1490478675.jpg', 'English Language Flag USA', 1, '2017-03-25 15:51:15', '2017-07-10 11:19:25'),
(2, 'Franchis', '1490478809.jpg', 'Franchish Language', 1, '2017-03-25 15:53:29', '2017-07-10 11:19:29'),
(3, 'Español', '1490478856.jpg', 'Español Language', 1, '2017-03-25 15:54:16', '2017-07-10 11:19:31'),
(4, 'Italiano', '1490478891.jpg', 'Etalian Language', 1, '2017-03-25 15:54:51', '2017-07-10 11:19:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_02_11_192044_create_sub_categories_table', 2),
('2017_02_12_115652_create_tags_table', 3),
('2017_02_14_195116_create_brands_table', 4),
('2017_02_16_083312_create_products_table', 5),
('2017_02_21_085341_create_product_tags_table', 6),
('2017_03_03_203312_create_orders_table', 9),
('2017_03_03_203900_create_orders_items_table', 9),
('2017_03_03_214821_create_order_emails_table', 10),
('2017_03_09_162600_create_payment_methods_table', 11),
('2017_03_09_165725_create_transactions_table', 12),
('2017_03_09_165937_create_order_transactions_table', 12),
('2017_03_11_201312_create_sliders_table', 13),
('2017_03_02_202935_create_seos_table', 14),
('2017_03_13_144309_create_carts_table', 14),
('2017_03_19_192954_create_reviews_table', 15),
('2017_03_25_200033_create_customer_supports_table', 16),
('2017_03_25_202922_create_contact_details_table', 17),
('2017_03_25_210743_create_languages_table', 18),
('2017_03_25_220303_create_currencies_table', 19),
('2017_03_28_184332_create_product_reviews_table', 20),
('2014_10_12_000000_create_users_table', 21),
('2014_10_12_100000_create_password_resets_table', 22),
('2016_10_29_065632_create_admin_table', 22),
('2017_01_26_190229_create_categories_table', 23),
('2017_03_02_210426_create_customers_table', 23),
('2017_03_29_110446_create_delivery_addresses_table', 23),
('2017_03_29_111157_create_invoice_addresses_table', 23),
('2017_04_10_203415_create_shipping_table', 24),
('2017_04_16_185629_create_orders_delivery_methods_table', 25),
('2017_04_16_185926_create_order_payment_methods_table', 26),
('2017_06_09_173125_create_wishlists_table', 27),
('2017_07_08_185834_create_contact_pages_table', 27),
('2018_04_14_200619_create_product_one_sub_levels_table', 28),
('2018_04_14_223216_create_product_two_sub_levels_table', 29),
('2018_04_19_174047_create_pizza_sizes_table', 30),
('2018_04_19_174101_create_pizza_flabours_table', 30);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `tracking` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `order_status` enum('Pending','Paid','Cancel') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `tracking`, `cart`, `cid`, `invoice_date`, `due_date`, `order_status`, `created_at`, `updated_at`) VALUES
(30, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:23:56', '2017-04-15 15:23:56'),
(31, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:24:52', '2017-04-15 15:24:52'),
(32, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:25:03', '2017-04-15 15:25:03'),
(33, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:25:41', '2017-04-15 15:25:41'),
(34, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:27:21', '2017-04-15 15:27:21'),
(35, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:28:15', '2017-04-15 15:28:15'),
(36, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:28:35', '2017-04-15 15:28:35'),
(37, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:28:56', '2017-04-15 15:28:56'),
(38, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:30:31', '2017-04-15 15:30:31'),
(39, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:30:45', '2017-04-15 15:30:45'),
(40, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 2, '2017-04-15', '2017-04-15', 'Pending', '2017-04-15 15:31:19', '2017-04-15 15:31:19'),
(41, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:21:14', '2017-04-16 12:21:14'),
(42, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:24:43', '2017-04-16 12:24:43'),
(43, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:25:40', '2017-04-16 12:25:40'),
(44, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:26:14', '2017-04-16 12:26:14'),
(45, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:26:38', '2017-04-16 12:26:38'),
(46, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:34:50', '2017-04-16 12:34:50'),
(47, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:36:50', '2017-04-16 12:36:50'),
(48, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:37:09', '2017-04-16 12:37:09'),
(49, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:37:46', '2017-04-16 12:37:46'),
(50, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:39:50', '2017-04-16 12:39:50'),
(51, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:40:19', '2017-04-16 12:40:19'),
(52, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:40:37', '2017-04-16 12:40:37'),
(53, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:41:53', '2017-04-16 12:41:53'),
(54, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:43:38', '2017-04-16 12:43:38'),
(55, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:44:46', '2017-04-16 12:44:46'),
(56, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:45:47', '2017-04-16 12:45:47'),
(57, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:46:38', '2017-04-16 12:46:38'),
(58, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:47:01', '2017-04-16 12:47:01'),
(59, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:47:37', '2017-04-16 12:47:37'),
(60, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:47:51', '2017-04-16 12:47:51'),
(61, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:48:07', '2017-04-16 12:48:07'),
(62, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 12:48:38', '2017-04-16 12:48:38'),
(63, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Paid', '2017-04-16 12:51:14', '2017-04-16 12:51:14'),
(64, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:07:41', '2017-04-16 13:07:41'),
(65, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Cancel', '2017-04-16 13:08:11', '2017-04-16 13:08:11'),
(66, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:10:47', '2017-04-16 13:10:47'),
(67, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:20:52', '2017-04-16 13:20:52'),
(68, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:22:27', '2017-04-16 13:22:27'),
(69, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Paid', '2017-04-16 13:24:03', '2017-04-16 13:24:03'),
(70, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:24:58', '2017-04-16 13:24:58'),
(71, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:43:10', '2017-04-16 13:43:10'),
(72, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Pending', '2017-04-16 13:51:05', '2017-04-16 13:51:05'),
(73, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, '2017-04-16', '2017-04-16', 'Cancel', '2017-04-16 14:21:18', '2017-04-16 14:21:18'),
(74, 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 2, '2017-04-17', '2017-04-17', 'Pending', '2017-04-17 11:36:51', '2017-04-17 11:36:51'),
(75, 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 2, '2017-05-01', '2017-05-01', 'Paid', '2017-05-01 12:22:36', '2017-07-20 14:42:22'),
(76, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 3, '2017-05-02', '2017-05-02', 'Paid', '2017-05-02 12:35:01', '2017-07-20 14:43:17'),
(77, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 2, '2017-05-02', '2017-05-02', 'Paid', '2017-05-02 13:14:34', '2017-07-20 14:48:01'),
(78, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 2, '2017-05-02', '2017-05-02', 'Paid', '2017-05-02 13:37:53', '2017-07-20 14:59:04'),
(79, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 2, '2017-05-02', '2017-05-02', 'Paid', '2017-05-02 13:42:40', '2017-07-20 15:40:50'),
(80, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 7, '2017-06-03', '2017-06-03', 'Cancel', '2017-06-03 11:18:40', '2017-07-22 12:19:11'),
(81, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 7, '2017-06-03', '2017-06-03', 'Pending', '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(82, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 7, '2017-06-04', '2017-06-04', 'Pending', '2017-06-04 11:43:42', '2017-06-04 11:43:42'),
(83, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 7, '2017-06-05', '2017-06-05', 'Paid', '2017-06-05 12:11:26', '2017-07-21 08:51:27'),
(84, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 9, '2017-06-07', '2017-06-07', 'Pending', '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(85, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 9, '2017-06-07', '2017-06-07', 'Pending', '2017-06-07 13:46:54', '2017-06-07 13:46:54'),
(86, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 9, '2017-06-07', '2017-06-07', 'Pending', '2017-06-07 13:47:34', '2017-06-07 13:47:34'),
(87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 10, '2017-06-07', '2017-06-07', 'Pending', '2017-06-07 13:55:40', '2017-06-07 13:55:40'),
(88, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 10, '2017-06-07', '2017-06-07', 'Pending', '2017-06-07 13:57:50', '2017-06-07 13:57:50'),
(89, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 10, '2017-06-09', '2017-06-09', 'Pending', '2017-06-09 11:07:21', '2017-06-09 11:07:21'),
(90, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 10, '2017-06-09', '2017-06-09', 'Pending', '2017-06-09 11:07:59', '2017-06-09 11:07:59'),
(91, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 10, '2017-06-09', '2017-06-09', 'Pending', '2017-06-09 11:08:48', '2017-06-09 11:08:48'),
(92, 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 10, '2017-06-09', '2017-06-09', 'Pending', '2017-06-09 13:47:31', '2017-06-09 13:47:31'),
(93, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 11, '2017-07-10', '2017-07-10', 'Pending', '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(94, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 11, '2017-07-13', '2017-07-13', 'Pending', '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(95, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(96, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(97, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(98, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(99, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(100, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(101, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:41:43', '2017-07-15 12:41:43'),
(102, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:56:18', '2017-07-15 12:56:18'),
(104, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 12:58:47', '2017-07-15 12:58:47'),
(105, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:01:13', '2017-07-15 13:01:13'),
(106, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:02:54', '2017-07-15 13:02:54'),
(107, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:04:07', '2017-07-15 13:04:07'),
(108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(109, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(110, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(111, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(112, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(113, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 11, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:17:01', '2017-07-15 13:17:01'),
(114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 12, '2017-07-15', '2017-07-15', 'Pending', '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(115, 'eSFB3', '', 1, '2017-07-12', '2017-07-05', 'Pending', '2017-07-19 08:59:13', '2017-07-19 08:59:13'),
(116, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 11, '2017-07-21', '2017-07-21', 'Pending', '2017-07-21 10:37:49', '2017-07-21 10:37:49'),
(117, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 11, '2017-07-21', '2017-07-21', 'Pending', '2017-07-21 10:38:02', '2017-07-21 10:38:02'),
(118, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 11, '2017-07-21', '2017-07-21', 'Pending', '2017-07-21 10:38:05', '2017-07-21 10:38:05'),
(119, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 11, '2017-07-22', '2017-07-22', 'Pending', '2017-07-22 11:36:47', '2017-07-22 11:36:47'),
(120, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 11, '2017-07-22', '2017-07-22', 'Pending', '2017-07-22 11:36:54', '2017-07-22 11:36:54'),
(121, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 11, '2017-07-22', '2017-07-22', 'Pending', '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(122, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 7, '2017-07-22', '2017-07-22', 'Pending', '2017-07-22 13:09:37', '2017-07-22 13:09:37'),
(123, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 7, '2017-07-22', '2017-07-22', 'Paid', '2017-07-22 13:10:07', '2017-07-23 11:52:21'),
(124, 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 08:59:01', '2017-07-23 08:59:01'),
(125, '1500822546', '1500822546', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 09:09:06', '2017-07-23 09:09:06'),
(126, '1500822573', '1500822573', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 09:09:33', '2017-07-23 09:09:33'),
(127, '1500822656', '1500822656', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 09:10:56', '2017-07-23 09:10:56'),
(128, '1500823608', '1500823608', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 09:26:49', '2017-07-23 09:26:49'),
(129, '1500824005', '1500824005', 7, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 09:33:25', '2017-07-23 09:33:25'),
(130, '1500825424', '1500825424', 7, '2017-07-23', '2017-07-23', 'Paid', '2017-07-23 09:57:04', '2017-07-23 10:44:43'),
(131, '1500826369', '1500826369', 7, '2017-07-23', '2017-07-23', 'Paid', '2017-07-23 10:12:49', '2017-07-23 10:44:12'),
(132, '1500827051', '1500827051', 11, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 10:24:11', '2017-07-23 10:24:11'),
(133, '1500827110', '1500827110', 11, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 10:25:10', '2017-07-23 10:25:10'),
(134, '1500827405', '1500827405', 11, '2017-07-23', '2017-07-23', 'Pending', '2017-07-23 10:30:15', '2017-07-23 10:30:15'),
(135, '1500895160', '1500895160', 28, '2017-07-24', '2017-07-24', 'Pending', '2017-07-24 05:20:22', '2017-07-24 05:20:22'),
(136, '1500927850', '1500927850', 27, '2017-07-24', '2017-07-24', 'Pending', '2017-07-24 14:24:23', '2017-07-24 14:24:23'),
(137, '1500927893', '1500927893', 27, '2017-07-24', '2017-07-24', 'Pending', '2017-07-24 14:25:00', '2017-07-24 14:25:00'),
(138, '1500928172', '1500928172', 11, '2017-07-24', '2017-07-24', 'Paid', '2017-07-24 14:29:43', '2017-07-24 14:32:49'),
(139, '1504863037', '1504863037', 7, '2017-09-08', '2017-09-08', 'Pending', '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(140, '1504863865', '1504863865', 7, '2017-09-08', '2017-09-08', 'Pending', '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(141, '1504892353', '1504892353', 7, '2017-09-08', '2017-09-08', 'Pending', '2017-09-08 11:39:19', '2017-09-08 11:39:19'),
(142, '1504893834', '1504893834', 7, '2017-09-08', '2017-09-08', 'Paid', '2017-09-08 12:04:14', '2017-09-08 12:39:59'),
(143, '1505156641', '1505156641', 29, '2017-09-11', '2017-09-11', 'Pending', '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(144, '1505832923', '1505832923', 30, '2017-09-19', '2017-09-19', 'Pending', '2017-09-19 19:58:29', '2017-09-19 19:58:29'),
(145, '1516822127', '1516822127', 7, '2018-01-24', '2018-01-24', 'Pending', '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(146, '1516990277', '1516990277', 7, '2018-01-26', '2018-01-26', 'Pending', '2018-01-26 12:14:35', '2018-01-26 12:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `ordersdeliverymethods`
--

CREATE TABLE `ordersdeliverymethods` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ordersdeliverymethods`
--

INSERT INTO `ordersdeliverymethods` (`id`, `token`, `shipping_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 64, '2017-04-16 13:07:41', '2017-04-16 13:07:41'),
(2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 65, '2017-04-16 13:08:11', '2017-04-16 13:08:11'),
(3, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 66, '2017-04-16 13:10:47', '2017-04-16 13:10:47'),
(4, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 67, '2017-04-16 13:20:52', '2017-04-16 13:20:52'),
(5, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 68, '2017-04-16 13:22:27', '2017-04-16 13:22:27'),
(6, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 69, '2017-04-16 13:24:03', '2017-04-16 13:24:03'),
(7, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 70, '2017-04-16 13:24:58', '2017-04-16 13:24:58'),
(8, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 71, '2017-04-16 13:43:10', '2017-04-16 13:43:10'),
(9, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 72, '2017-04-16 13:51:06', '2017-04-16 13:51:06'),
(10, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 73, '2017-04-16 14:21:19', '2017-04-16 14:21:19'),
(11, 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 1, 74, '2017-04-17 11:36:51', '2017-04-17 11:36:51'),
(12, 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 1, 75, '2017-05-01 12:22:37', '2017-05-01 12:22:37'),
(13, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 1, 76, '2017-05-02 12:35:01', '2017-05-02 12:35:01'),
(14, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 1, 77, '2017-05-02 13:14:35', '2017-05-02 13:14:35'),
(15, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 1, 78, '2017-05-02 13:37:54', '2017-05-02 13:37:54'),
(16, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 1, 79, '2017-05-02 13:42:40', '2017-05-02 13:42:40'),
(17, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 1, 80, '2017-06-03 11:18:40', '2017-06-03 11:18:40'),
(18, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 1, 81, '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(19, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 1, 82, '2017-06-04 11:43:43', '2017-06-04 11:43:43'),
(20, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 1, 83, '2017-06-05 12:11:26', '2017-06-05 12:11:26'),
(21, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 1, 84, '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(22, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 2, 85, '2017-06-07 13:46:55', '2017-06-07 13:46:55'),
(23, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 2, 86, '2017-06-07 13:47:34', '2017-06-07 13:47:34'),
(24, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 2, 87, '2017-06-07 13:55:41', '2017-06-07 13:55:41'),
(25, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 1, 88, '2017-06-07 13:57:50', '2017-06-07 13:57:50'),
(26, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 1, 89, '2017-06-09 11:07:21', '2017-06-09 11:07:21'),
(27, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 1, 90, '2017-06-09 11:07:59', '2017-06-09 11:07:59'),
(28, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 1, 91, '2017-06-09 11:08:48', '2017-06-09 11:08:48'),
(29, 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 1, 92, '2017-06-09 13:47:31', '2017-06-09 13:47:31'),
(30, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 2, 93, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(31, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 2, 94, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(32, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 95, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(33, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 96, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(34, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 97, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(35, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 98, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(36, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 99, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(37, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 100, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(38, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 101, '2017-07-15 12:41:43', '2017-07-15 12:41:43'),
(39, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 102, '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(40, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 103, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(41, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 104, '2017-07-15 12:58:48', '2017-07-15 12:58:48'),
(42, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 105, '2017-07-15 13:01:14', '2017-07-15 13:01:14'),
(43, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 106, '2017-07-15 13:02:54', '2017-07-15 13:02:54'),
(44, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 107, '2017-07-15 13:04:07', '2017-07-15 13:04:07'),
(45, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 108, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(46, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 109, '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(47, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 110, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(48, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 111, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(49, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 112, '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(50, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 113, '2017-07-15 13:17:01', '2017-07-15 13:17:01'),
(51, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 2, 114, '2017-07-15 13:27:08', '2017-07-15 13:27:08'),
(52, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 116, '2017-07-21 10:37:49', '2017-07-21 10:37:49'),
(53, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 117, '2017-07-21 10:38:02', '2017-07-21 10:38:02'),
(54, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 118, '2017-07-21 10:38:05', '2017-07-21 10:38:05'),
(55, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 2, 119, '2017-07-22 11:36:47', '2017-07-22 11:36:47'),
(56, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 2, 120, '2017-07-22 11:36:54', '2017-07-22 11:36:54'),
(57, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 1, 121, '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(58, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 1, 122, '2017-07-22 13:09:37', '2017-07-22 13:09:37'),
(59, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 1, 123, '2017-07-22 13:10:07', '2017-07-22 13:10:07'),
(60, 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 1, 124, '2017-07-23 08:59:02', '2017-07-23 08:59:02'),
(61, '1500822546', 1, 125, '2017-07-23 09:09:06', '2017-07-23 09:09:06'),
(62, '1500822573', 2, 126, '2017-07-23 09:09:33', '2017-07-23 09:09:33'),
(63, '1500822656', 2, 127, '2017-07-23 09:10:56', '2017-07-23 09:10:56'),
(64, '1500823608', 1, 128, '2017-07-23 09:26:49', '2017-07-23 09:26:49'),
(65, '1500824005', 2, 129, '2017-07-23 09:33:26', '2017-07-23 09:33:26'),
(66, '1500825424', 2, 130, '2017-07-23 09:57:05', '2017-07-23 09:57:05'),
(67, '1500826369', 2, 131, '2017-07-23 10:12:49', '2017-07-23 10:12:49'),
(68, '1500827051', 2, 132, '2017-07-23 10:24:11', '2017-07-23 10:24:11'),
(69, '1500827110', 2, 133, '2017-07-23 10:25:11', '2017-07-23 10:25:11'),
(70, '1500827405', 2, 134, '2017-07-23 10:30:15', '2017-07-23 10:30:15'),
(71, '1500895160', 2, 135, '2017-07-24 05:20:22', '2017-07-24 05:20:22'),
(72, '1500927850', 1, 136, '2017-07-24 14:24:23', '2017-07-24 14:24:23'),
(73, '1500927893', 2, 137, '2017-07-24 14:25:00', '2017-07-24 14:25:00'),
(74, '1500928172', 1, 138, '2017-07-24 14:29:43', '2017-07-24 14:29:43'),
(75, '1504863037', 1, 139, '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(76, '1504863865', 1, 140, '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(77, '1504892353', 1, 141, '2017-09-08 11:39:20', '2017-09-08 11:39:20'),
(78, '1504893834', 1, 142, '2017-09-08 12:04:14', '2017-09-08 12:04:14'),
(79, '1505156641', 2, 143, '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(80, '1505832923', 2, 144, '2017-09-19 19:58:29', '2017-09-19 19:58:29'),
(81, '1516822127', 1, 145, '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(82, '1516990277', 1, 146, '2018-01-26 12:14:35', '2018-01-26 12:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE `orders_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `tracking` text COLLATE utf8_unicode_ci NOT NULL,
  `pid` int(11) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `unit` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` double(8,2) NOT NULL,
  `tax_rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_amount` double(8,2) NOT NULL,
  `row_total` double(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders_items`
--

INSERT INTO `orders_items` (`id`, `order_id`, `tracking`, `pid`, `quantity`, `unit`, `color`, `unit_price`, `tax_rate`, `tax_amount`, `row_total`, `created_at`, `updated_at`) VALUES
(1, 5, 'uxqAG', 2, 5.00, '', '', 10.00, '10', 5.00, 45.00, '2017-03-03 15:05:13', '2017-03-03 15:05:13'),
(2, 5, 'uxqAG', 0, 4.00, '', '', 15.00, '10', 5.00, 55.00, '2017-03-03 15:05:13', '2017-03-03 15:05:13'),
(3, 6, 'uxqAG', 2, 5.00, '', '', 10.00, '10', 5.00, 45.00, '2017-03-03 15:05:59', '2017-03-03 15:05:59'),
(4, 6, 'uxqAG', 0, 4.00, '', '', 15.00, '10', 5.00, 55.00, '2017-03-03 15:05:59', '2017-03-03 15:05:59'),
(5, 7, 'uxqAG', 2, 5.00, '', '', 10.00, '10', 5.00, 45.00, '2017-03-03 15:06:52', '2017-03-03 15:06:52'),
(6, 8, 'uxqAG', 2, 5.00, '', '', 10.00, '10', 5.00, 45.00, '2017-03-03 15:08:32', '2017-03-03 15:08:32'),
(7, 9, 'oIWbq', 2, 2.00, '', '', 10.00, '1', 2.00, 18.00, '2017-03-09 05:34:48', '2017-03-09 05:34:48'),
(8, 29, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 0, 0.00, '', '', 0.00, '', 0.00, 0.00, '2017-04-15 15:20:22', '2017-04-15 15:20:22'),
(9, 29, 'sH9sgqOZwHX2w7lW05Qpnb1UfYKfCnxjy3RMJYZI', 0, 0.00, '', '', 0.00, '', 0.00, 0.00, '2017-04-15 15:20:22', '2017-04-15 15:20:22'),
(10, 63, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 12:51:14', '2017-04-16 12:51:14'),
(11, 64, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:07:41', '2017-04-16 13:07:41'),
(12, 65, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:08:11', '2017-04-16 13:08:11'),
(13, 66, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:10:47', '2017-04-16 13:10:47'),
(14, 67, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:20:52', '2017-04-16 13:20:52'),
(15, 68, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:22:27', '2017-04-16 13:22:27'),
(16, 69, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:24:03', '2017-04-16 13:24:03'),
(17, 70, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:24:58', '2017-04-16 13:24:58'),
(18, 71, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:43:10', '2017-04-16 13:43:10'),
(19, 72, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 13:51:05', '2017-04-16 13:51:05'),
(20, 73, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 36, 2.00, '', '', 225.26, '', 0.00, 450.52, '2017-04-16 14:21:19', '2017-04-16 14:21:19'),
(21, 74, 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 36, 1.00, '', '', 225.26, '', 0.00, 225.26, '2017-04-17 11:36:51', '2017-04-17 11:36:51'),
(22, 75, 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 22, 13.00, '', '', 2.68, '', 0.00, 34.84, '2017-05-01 12:22:37', '2017-05-01 12:22:37'),
(23, 76, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-05-02 12:35:01', '2017-05-02 12:35:01'),
(24, 76, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-05-02 12:35:01', '2017-05-02 12:35:01'),
(25, 77, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-05-02 13:14:34', '2017-05-02 13:14:34'),
(26, 77, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-05-02 13:14:34', '2017-05-02 13:14:34'),
(27, 78, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-05-02 13:37:53', '2017-05-02 13:37:53'),
(28, 78, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-05-02 13:37:54', '2017-05-02 13:37:54'),
(29, 79, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-05-02 13:42:40', '2017-05-02 13:42:40'),
(30, 79, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-05-02 13:42:40', '2017-05-02 13:42:40'),
(31, 80, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-06-03 11:18:40', '2017-06-03 11:18:40'),
(32, 80, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-06-03 11:18:40', '2017-06-03 11:18:40'),
(33, 81, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(34, 81, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(35, 81, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(36, 82, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-06-04 11:43:42', '2017-06-04 11:43:42'),
(37, 82, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-06-04 11:43:43', '2017-06-04 11:43:43'),
(38, 82, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-06-04 11:43:43', '2017-06-04 11:43:43'),
(39, 83, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-06-05 12:11:26', '2017-06-05 12:11:26'),
(40, 83, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-06-05 12:11:26', '2017-06-05 12:11:26'),
(41, 84, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 17, 8.00, '', '', 4.00, '', 0.00, 32.00, '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(42, 85, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-06-07 13:46:55', '2017-06-07 13:46:55'),
(43, 85, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-06-07 13:46:55', '2017-06-07 13:46:55'),
(44, 86, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-06-07 13:47:34', '2017-06-07 13:47:34'),
(45, 87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-06-07 13:55:40', '2017-06-07 13:55:40'),
(46, 87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 25, 1.00, '', '', 4.52, '', 0.00, 4.52, '2017-06-07 13:55:40', '2017-06-07 13:55:40'),
(47, 87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-06-07 13:55:41', '2017-06-07 13:55:41'),
(48, 87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 27, 1.00, '', '', 75.78, '', 0.00, 75.78, '2017-06-07 13:55:41', '2017-06-07 13:55:41'),
(49, 87, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 28, 1.00, '', '', 3.45, '', 0.00, 3.45, '2017-06-07 13:55:41', '2017-06-07 13:55:41'),
(50, 88, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 32, 1.00, '', '', 1889.00, '', 0.00, 1889.00, '2017-06-07 13:57:50', '2017-06-07 13:57:50'),
(51, 89, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-06-09 11:07:21', '2017-06-09 11:07:21'),
(52, 90, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-06-09 11:07:59', '2017-06-09 11:07:59'),
(53, 91, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-06-09 11:08:48', '2017-06-09 11:08:48'),
(54, 92, 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 16, 3.00, '', '', 5.50, '', 0.00, 16.50, '2017-06-09 13:47:31', '2017-06-09 13:47:31'),
(55, 93, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(56, 93, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(57, 93, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(58, 94, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(59, 94, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(60, 94, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(61, 94, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(62, 95, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(63, 95, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(64, 95, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(65, 95, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(66, 96, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(67, 96, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(68, 96, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(69, 96, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(70, 97, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(71, 98, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(72, 97, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(73, 98, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(74, 97, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(75, 98, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(76, 97, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(77, 98, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:45', '2017-07-15 12:39:45'),
(78, 99, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(79, 99, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(80, 99, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(81, 99, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(82, 100, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(83, 100, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(84, 100, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(85, 100, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(86, 101, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 27, 1.00, '', '', 75.78, '', 0.00, 75.78, '2017-07-15 12:41:43', '2017-07-15 12:41:43'),
(87, 102, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(88, 102, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(89, 102, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(90, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 22, 4.00, '', '', 2.68, '', 0.00, 10.72, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(91, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 5.00, '', '', 19.92, '', 0.00, 99.60, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(92, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(93, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 27, 4.00, '', '', 75.78, '', 0.00, 303.12, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(94, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 26, 2.00, '', '', 30.02, '', 0.00, 60.04, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(95, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 25, 2.00, '', '', 4.52, '', 0.00, 9.04, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(96, 103, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 28, 1.00, '', '', 3.45, '', 0.00, 3.45, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(97, 104, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 12:58:48', '2017-07-15 12:58:48'),
(98, 104, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-15 12:58:48', '2017-07-15 12:58:48'),
(99, 104, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-07-15 12:58:48', '2017-07-15 12:58:48'),
(100, 105, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 27, 1.00, '', '', 75.78, '', 0.00, 75.78, '2017-07-15 13:01:13', '2017-07-15 13:01:13'),
(101, 105, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-07-15 13:01:13', '2017-07-15 13:01:13'),
(102, 105, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 25, 1.00, '', '', 4.52, '', 0.00, 4.52, '2017-07-15 13:01:13', '2017-07-15 13:01:13'),
(103, 106, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 13:02:54', '2017-07-15 13:02:54'),
(104, 107, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 13:04:07', '2017-07-15 13:04:07'),
(105, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(106, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(107, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(108, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(109, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(110, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(111, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(112, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 27, 1.00, '', '', 75.78, '', 0.00, 75.78, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(113, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(114, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 25, 1.00, '', '', 4.52, '', 0.00, 4.52, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(115, 108, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 28, 1.00, '', '', 3.45, '', 0.00, 3.45, '2017-07-15 13:05:52', '2017-07-15 13:05:52'),
(116, 109, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(117, 109, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(118, 110, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 7.00, '', '', 4.00, '', 0.00, 28.00, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(119, 110, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(120, 110, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(121, 110, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(122, 111, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 7.00, '', '', 4.00, '', 0.00, 28.00, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(123, 111, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(124, 111, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(125, 111, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(126, 112, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(127, 112, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(128, 113, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 13:17:01', '2017-07-15 13:17:01'),
(129, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(130, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(131, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 15, 1.00, '', '', 15.50, '', 0.00, 15.50, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(132, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(133, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 24, 1.00, '', '', 41.56, '', 0.00, 41.56, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(134, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(135, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(136, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 27, 1.00, '', '', 75.78, '', 0.00, 75.78, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(137, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(138, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 25, 1.00, '', '', 4.52, '', 0.00, 4.52, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(139, 114, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 28, 1.00, '', '', 3.45, '', 0.00, 3.45, '2017-07-15 13:27:07', '2017-07-15 13:27:07'),
(140, 115, 'eSFB3', 28, 54.00, '', '', 3.45, '0', 0.00, 186.30, '2017-07-19 08:59:14', '2017-07-19 08:59:14'),
(141, 115, 'eSFB3', 30, 54.00, '', '', 373.74, '0', 0.00, 186.30, '2017-07-19 08:59:14', '2017-07-19 08:59:14'),
(142, 116, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 17, 2.00, '', '', 4.00, '', 0.00, 8.00, '2017-07-21 10:37:49', '2017-07-21 10:37:49'),
(143, 117, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 17, 2.00, '', '', 4.00, '', 0.00, 8.00, '2017-07-21 10:38:02', '2017-07-21 10:38:02'),
(144, 118, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 17, 2.00, '', '', 4.00, '', 0.00, 8.00, '2017-07-21 10:38:05', '2017-07-21 10:38:05'),
(145, 119, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-22 11:36:47', '2017-07-22 11:36:47'),
(146, 120, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-22 11:36:54', '2017-07-22 11:36:54'),
(147, 121, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(148, 121, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(149, 122, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 22, 2.00, '', '', 2.68, '', 0.00, 5.36, '2017-07-22 13:09:37', '2017-07-22 13:09:37'),
(150, 123, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-22 13:10:07', '2017-07-22 13:10:07'),
(151, 124, 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-23 08:59:02', '2017-07-23 08:59:02'),
(152, 125, '1500822546', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:09:06', '2017-07-23 09:09:06'),
(153, 126, '1500822573', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:09:33', '2017-07-23 09:09:33'),
(154, 127, '1500822656', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:10:56', '2017-07-23 09:10:56'),
(155, 128, '1500823608', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:26:49', '2017-07-23 09:26:49'),
(156, 129, '1500824005', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:33:26', '2017-07-23 09:33:26'),
(157, 130, '1500825424', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 09:57:05', '2017-07-23 09:57:05'),
(158, 131, '1500826369', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-23 10:12:49', '2017-07-23 10:12:49'),
(159, 132, '1500827051', 17, 1.00, '', '', 4.00, '', 0.00, 4.00, '2017-07-23 10:24:11', '2017-07-23 10:24:11'),
(160, 133, '1500827110', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-23 10:25:11', '2017-07-23 10:25:11'),
(161, 134, '1500827405', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-07-23 10:30:15', '2017-07-23 10:30:15'),
(162, 135, '1500895160', 16, 1.00, '', '', 5.50, '', 0.00, 5.50, '2017-07-24 05:20:22', '2017-07-24 05:20:22'),
(163, 136, '1500927850', 22, 1.00, '', '', 2.68, '', 0.00, 2.68, '2017-07-24 14:24:23', '2017-07-24 14:24:23'),
(164, 137, '1500927893', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-07-24 14:25:00', '2017-07-24 14:25:00'),
(165, 138, '1500928172', 23, 1.00, '', '', 19.92, '', 0.00, 19.92, '2017-07-24 14:29:43', '2017-07-24 14:29:43'),
(166, 139, '1504863037', 49, 2.00, '', '', 100.00, '', 0.00, 200.00, '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(167, 139, '1504863037', 26, 1.00, '', '', 30.02, '', 0.00, 30.02, '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(168, 140, '1504863865', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(169, 140, '1504863865', 49, 1.00, '2', 'Green', 100.00, '', 0.00, 100.00, '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(170, 141, '1504892353', 49, 2.00, '2', 'Blue', 100.00, '', 0.00, 200.00, '2017-09-08 11:39:20', '2017-09-08 11:39:20'),
(171, 142, '1504893834', 49, 1.00, '2 KG', 'Blue', 100.00, '', 0.00, 100.00, '2017-09-08 12:04:14', '2017-09-08 12:04:14'),
(172, 142, '1504893834', 14, 1.00, '', '', 8.50, '', 0.00, 8.50, '2017-09-08 12:04:14', '2017-09-08 12:04:14'),
(173, 143, '1505156641', 20, 1.00, '', '', 40.50, '', 0.00, 40.50, '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(174, 143, '1505156641', 29, 1.00, '', '', 1799.00, '', 0.00, 1799.00, '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(175, 144, '1505832923', 50, 1.00, ' 2 Litter', 'Green', 12.00, '', 0.00, 12.00, '2017-09-19 19:58:29', '2017-09-19 19:58:29'),
(176, 145, '1516822127', 53, 1.00, '', '', 4.84, '', 0.00, 4.84, '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(177, 145, '1516822127', 1835, 1.00, '', '', 7.96, '', 0.00, 7.96, '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(178, 146, '1516990277', 1835, 1.00, '', '', 7.96, '', 0.00, 7.96, '2018-01-26 12:14:35', '2018-01-26 12:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_emails`
--

CREATE TABLE `order_emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `primary_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `backup_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_infos`
--

CREATE TABLE `order_infos` (
  `id` int(11) NOT NULL,
  `delivery_time` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `allergy_alert` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_infos`
--

INSERT INTO `order_infos` (`id`, `delivery_time`, `allergy_alert`, `delivery_info`, `description`, `isactive`, `created_at`, `updated_at`) VALUES
(9, 'cccc332', 'cacacascac', 'cacascas', 'cacac', 1, '2018-04-03 10:38:38', '2018-04-03 04:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `order_payment_methods`
--

CREATE TABLE `order_payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_payment_methods`
--

INSERT INTO `order_payment_methods` (`id`, `token`, `payment_method_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 65, '2017-04-16 13:08:11', '2017-04-16 13:08:11'),
(2, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 66, '2017-04-16 13:10:47', '2017-04-16 13:10:47'),
(3, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 1, 67, '2017-04-16 13:20:52', '2017-04-16 13:20:52'),
(4, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 68, '2017-04-16 13:22:27', '2017-04-16 13:22:27'),
(5, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 69, '2017-04-16 13:24:03', '2017-04-16 13:24:03'),
(6, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 70, '2017-04-16 13:24:58', '2017-04-16 13:24:58'),
(7, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 71, '2017-04-16 13:43:10', '2017-04-16 13:43:10'),
(8, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 72, '2017-04-16 13:51:06', '2017-04-16 13:51:06'),
(9, '7CjB40ldwXZoSCzIvq4YEM38mPH5BVaf66v5pFCe', 2, 73, '2017-04-16 14:21:19', '2017-04-16 14:21:19'),
(10, 'JMl9ZqH2WmeUnrVxyeRigdOlSGpWcb4iSyDm9uyk', 2, 74, '2017-04-17 11:36:51', '2017-04-17 11:36:51'),
(11, 'jftBe6xZ1kRFq07RNPtz7p6fbpH441KMkAw6hTRJ', 2, 75, '2017-05-01 12:22:37', '2017-05-01 12:22:37'),
(12, 'vu6KEK5pwUsw4cAtL1C3k60zmY9ye9xsWxuxNm7d', 1, 76, '2017-05-02 12:35:02', '2017-05-02 12:35:02'),
(13, 'n51J69L7tJ9Pn5x04qNSwfYtp4XQrN0lXgfWYg0U', 1, 77, '2017-05-02 13:14:35', '2017-05-02 13:14:35'),
(14, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 1, 78, '2017-05-02 13:37:54', '2017-05-02 13:37:54'),
(15, 'vg5jzuljkLTZgBE7hCUnl5NWb3ZV85VmgesSjCsq', 1, 79, '2017-05-02 13:42:40', '2017-05-02 13:42:40'),
(16, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 1, 80, '2017-06-03 11:18:40', '2017-06-03 11:18:40'),
(17, 'ZOkEU8EJFgvLZqMxZAGIq6QrBhsQopUTKNTHxX4K', 1, 81, '2017-06-03 11:23:07', '2017-06-03 11:23:07'),
(18, '7clTuwFlK5aiCmkPOJQPA7v5TKE4eTV3PeP57LCK', 1, 82, '2017-06-04 11:43:43', '2017-06-04 11:43:43'),
(19, 'x3dQ8rL9Teo63Yt2DzJY1f55meNHRnREEffc8LqY', 1, 83, '2017-06-05 12:11:26', '2017-06-05 12:11:26'),
(20, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 1, 84, '2017-06-07 13:45:34', '2017-06-07 13:45:34'),
(21, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 2, 85, '2017-06-07 13:46:55', '2017-06-07 13:46:55'),
(22, '0PxQWLhugAsuLzyTGDlXMcaAqGcfE8rHOuTIhUVG', 2, 86, '2017-06-07 13:47:34', '2017-06-07 13:47:34'),
(23, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 3, 87, '2017-06-07 13:55:41', '2017-06-07 13:55:41'),
(24, 'pp8OcJ7t7LPomGfAtKRuYkeeidgujkdC5i0ripgd', 3, 88, '2017-06-07 13:57:50', '2017-06-07 13:57:50'),
(25, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 2, 89, '2017-06-09 11:07:21', '2017-06-09 11:07:21'),
(26, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 1, 90, '2017-06-09 11:07:59', '2017-06-09 11:07:59'),
(27, 'Lv5jKyk3vqJNYNZpzLM8J0Mq1gId7PVlLQuLqjKC', 1, 91, '2017-06-09 11:08:48', '2017-06-09 11:08:48'),
(28, 'i1KkAA4DtlDMwJrWfVsGEgAIeNh6FHMmWEhS2KkJ', 1, 92, '2017-06-09 13:47:31', '2017-06-09 13:47:31'),
(29, 'bUMMERAePc8nvHKz6L91HBIM17LgTTw2MxRWR8Ox', 1, 93, '2017-07-10 01:26:37', '2017-07-10 01:26:37'),
(30, 'ji8ZVehOduvx6EeHsa8f6sJElWc4c0vm6FfTUQYz', 2, 94, '2017-07-13 01:16:46', '2017-07-13 01:16:46'),
(31, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 95, '2017-07-15 12:39:40', '2017-07-15 12:39:40'),
(32, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 96, '2017-07-15 12:39:44', '2017-07-15 12:39:44'),
(33, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 97, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(34, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 98, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(35, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 99, '2017-07-15 12:39:46', '2017-07-15 12:39:46'),
(36, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 100, '2017-07-15 12:39:47', '2017-07-15 12:39:47'),
(37, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 101, '2017-07-15 12:41:43', '2017-07-15 12:41:43'),
(38, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 102, '2017-07-15 12:53:26', '2017-07-15 12:53:26'),
(39, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 103, '2017-07-15 12:56:19', '2017-07-15 12:56:19'),
(40, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 104, '2017-07-15 12:58:48', '2017-07-15 12:58:48'),
(41, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 105, '2017-07-15 13:01:14', '2017-07-15 13:01:14'),
(42, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 106, '2017-07-15 13:02:54', '2017-07-15 13:02:54'),
(43, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 107, '2017-07-15 13:04:08', '2017-07-15 13:04:08'),
(44, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 108, '2017-07-15 13:05:53', '2017-07-15 13:05:53'),
(45, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 109, '2017-07-15 13:08:05', '2017-07-15 13:08:05'),
(46, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 110, '2017-07-15 13:11:03', '2017-07-15 13:11:03'),
(47, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 1, 111, '2017-07-15 13:11:22', '2017-07-15 13:11:22'),
(48, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 3, 112, '2017-07-15 13:12:04', '2017-07-15 13:12:04'),
(49, 'YNuctgSP1hl5iSkX2Ck5Y9Jy6940Bd291mSTkIfK', 2, 113, '2017-07-15 13:17:02', '2017-07-15 13:17:02'),
(50, 'OjTG0GrzGlhlvs4liTTM28a9vSeLWaM6vd1qeEfI', 1, 114, '2017-07-15 13:27:08', '2017-07-15 13:27:08'),
(51, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 116, '2017-07-21 10:37:49', '2017-07-21 10:37:49'),
(52, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 117, '2017-07-21 10:38:02', '2017-07-21 10:38:02'),
(53, 'eQa7MH0VIkwnuCzLaCl9mfRXRieQXYn5nd7hpgOQ', 2, 118, '2017-07-21 10:38:05', '2017-07-21 10:38:05'),
(54, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 2, 119, '2017-07-22 11:36:48', '2017-07-22 11:36:48'),
(55, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 2, 120, '2017-07-22 11:36:54', '2017-07-22 11:36:54'),
(56, 'v6oHjfVBd1UCsE3TvdtOhlEb3PhU9uhp1mRm6mrQ', 2, 121, '2017-07-22 11:40:03', '2017-07-22 11:40:03'),
(57, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 1, 122, '2017-07-22 13:09:37', '2017-07-22 13:09:37'),
(58, 'k9U68SyA9y12him8EXZN0o1T8KoGELqtIIQSGye7', 2, 123, '2017-07-22 13:10:07', '2017-07-22 13:10:07'),
(59, 'YbXRQGNM4cjqqcarKBToyRae23AfuFFQRuyZ2pRX', 2, 124, '2017-07-23 08:59:02', '2017-07-23 08:59:02'),
(60, '1500822546', 2, 125, '2017-07-23 09:09:06', '2017-07-23 09:09:06'),
(61, '1500822573', 2, 126, '2017-07-23 09:09:33', '2017-07-23 09:09:33'),
(62, '1500822656', 2, 127, '2017-07-23 09:10:56', '2017-07-23 09:10:56'),
(63, '1500823608', 2, 128, '2017-07-23 09:26:49', '2017-07-23 09:26:49'),
(64, '1500824005', 2, 129, '2017-07-23 09:33:26', '2017-07-23 09:33:26'),
(65, '1500825424', 2, 130, '2017-07-23 09:57:05', '2017-07-23 09:57:05'),
(66, '1500826369', 2, 131, '2017-07-23 10:12:49', '2017-07-23 10:12:49'),
(67, '1500827051', 2, 132, '2017-07-23 10:24:11', '2017-07-23 10:24:11'),
(68, '1500827110', 1, 133, '2017-07-23 10:25:11', '2017-07-23 10:25:11'),
(69, '1500827405', 1, 134, '2017-07-23 10:30:15', '2017-07-23 10:30:15'),
(70, '1500895160', 3, 135, '2017-07-24 05:20:22', '2017-07-24 05:20:22'),
(71, '1500927850', 1, 136, '2017-07-24 14:24:23', '2017-07-24 14:24:23'),
(72, '1500927893', 2, 137, '2017-07-24 14:25:00', '2017-07-24 14:25:00'),
(73, '1500928172', 2, 138, '2017-07-24 14:29:43', '2017-07-24 14:29:43'),
(74, '1504863037', 1, 139, '2017-09-08 03:31:05', '2017-09-08 03:31:05'),
(75, '1504863865', 2, 140, '2017-09-08 03:44:38', '2017-09-08 03:44:38'),
(76, '1504892353', 2, 141, '2017-09-08 11:39:20', '2017-09-08 11:39:20'),
(77, '1504893834', 2, 142, '2017-09-08 12:04:14', '2017-09-08 12:04:14'),
(78, '1505156641', 1, 143, '2017-09-12 00:05:03', '2017-09-12 00:05:03'),
(79, '1505832923', 1, 144, '2017-09-19 19:58:29', '2017-09-19 19:58:29'),
(80, '1516822127', 3, 145, '2018-01-24 13:29:13', '2018-01-24 13:29:13'),
(81, '1516990277', 1, 146, '2018-01-26 12:14:35', '2018-01-26 12:14:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_transactions`
--

CREATE TABLE `order_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `pm` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `status` enum('Completed','Failed') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_transactions`
--

INSERT INTO `order_transactions` (`id`, `order_id`, `pm`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 20.00, 'Completed', '2017-03-09 11:19:14', '2017-03-09 11:19:14'),
(2, 9, 1, 20.00, 'Completed', '2017-03-09 11:20:26', '2017-03-09 11:20:26'),
(3, 9, 1, 20.00, 'Completed', '2017-03-09 11:21:01', '2017-03-09 11:21:01'),
(4, 8, 1, 50.00, 'Completed', '2017-03-09 12:17:03', '2017-03-09 12:17:03'),
(5, 8, 1, 50.00, 'Completed', '2017-03-09 12:17:29', '2017-03-09 12:17:29'),
(6, 7, 1, 50.00, 'Completed', '2017-03-09 12:18:12', '2017-03-09 12:18:12'),
(7, 7, 1, 50.00, 'Completed', '2017-03-09 12:19:51', '2017-03-09 12:19:51'),
(8, 82, 1, 64.16, 'Completed', '2017-07-10 11:27:39', '2017-07-10 11:27:39'),
(9, 115, 1, 372.60, 'Completed', '2017-07-20 14:30:42', '2017-07-20 14:30:42'),
(10, 75, 1, 34.00, 'Completed', '2017-07-20 14:31:44', '2017-07-20 14:31:44'),
(11, 75, 1, 34.00, 'Completed', '2017-07-20 14:42:22', '2017-07-20 14:42:22'),
(12, 76, 0, 24.00, 'Completed', '2017-07-20 14:43:17', '2017-07-20 14:43:17'),
(13, 76, 1, 24.00, 'Completed', '2017-07-20 14:43:42', '2017-07-20 14:43:42'),
(14, 77, 1, 24.00, 'Completed', '2017-07-20 14:48:07', '2017-07-20 14:48:07'),
(15, 78, 1, 24.00, 'Completed', '2017-07-20 15:00:58', '2017-07-20 15:00:58'),
(16, 78, 1, 24.00, 'Completed', '2017-07-20 15:03:24', '2017-07-20 15:03:24'),
(17, 78, 1, 24.00, 'Completed', '2017-07-20 15:27:16', '2017-07-20 15:27:16'),
(18, 78, 1, 24.00, 'Completed', '2017-07-20 15:29:43', '2017-07-20 15:29:43'),
(19, 79, 2, 24.00, 'Completed', '2017-07-20 15:40:58', '2017-07-20 15:40:58'),
(20, 79, 2, 24.00, 'Completed', '2017-07-20 15:57:42', '2017-07-20 15:57:42'),
(21, 83, 3, 24.00, 'Completed', '2017-07-21 08:51:35', '2017-07-21 08:51:35'),
(22, 83, 3, 24.00, 'Completed', '2017-07-21 08:53:25', '2017-07-21 08:53:25'),
(23, 80, 1, 24.00, 'Completed', '2017-07-22 12:19:11', '2017-07-22 12:19:11'),
(24, 131, 1, 3.00, 'Completed', '2017-07-23 10:44:18', '2017-07-23 10:44:18'),
(25, 130, 2, 3.00, 'Completed', '2017-07-23 10:44:43', '2017-07-23 10:44:43'),
(26, 123, 1, 2.68, 'Completed', '2017-07-23 11:52:35', '2017-07-23 11:52:35'),
(27, 138, 3, 19.92, 'Completed', '2017-07-24 14:32:54', '2017-07-24 14:32:54'),
(28, 142, 2, 108.50, 'Completed', '2017-09-08 12:40:05', '2017-09-08 12:40:05'),
(29, 142, 2, 108.50, 'Completed', '2017-09-08 12:41:08', '2017-09-08 12:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `description`, `status`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'Cash On Delivery', 'Your Product will be on your hand and payment will be cash to our authorized supplier.', 'Active', 0, '2017-03-08 18:00:00', '2018-03-31 00:48:34'),
(2, 'Online', 'You Can Choose Several Payment Options VISA, Credit Card, Master Card', 'Active', 0, '2017-04-13 11:36:57', '2017-04-13 11:36:57'),
(3, 'Paypal', 'Either You can Choose Paypal.', 'Active', 0, '2017-04-13 11:37:17', '2017-04-13 11:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `pizza_flabours`
--

CREATE TABLE `pizza_flabours` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pizza_flabours`
--

INSERT INTO `pizza_flabours` (`id`, `name`, `price`, `pid`, `created_at`, `updated_at`) VALUES
(1, 'Normal', '0', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(2, 'Spicy', '0', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(3, 'Extra Spicy', '0', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(4, 'Normal', '0', 4, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(5, 'Spicy', '0', 4, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(6, 'Very Spicy', '0', 4, '2018-04-19 12:48:28', '2018-04-19 12:48:28'),
(7, 'Small Pan', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47'),
(8, 'Regular Thin', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47'),
(9, 'Large Crust', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `pizza_sizes`
--

CREATE TABLE `pizza_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pizza_sizes`
--

INSERT INTO `pizza_sizes` (`id`, `name`, `price`, `pid`, `created_at`, `updated_at`) VALUES
(1, 'Small', '5.59', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(2, 'Medium', '7.25', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(3, 'Large', '10.50', 13, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(4, 'Small', '5.30', 4, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(5, 'Medium', '4.00', 4, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(6, 'Small', '3.25', 4, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(7, 'Small', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47'),
(8, 'Medium', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47'),
(9, 'Large', '0', 16, '2018-04-23 13:41:47', '2018-04-23 13:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `pcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_level_type` int(2) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(20) NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `isactive` tinyint(1) NOT NULL,
  `position` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pcode`, `product_level_type`, `name`, `quantity`, `price`, `old_price`, `cid`, `scid`, `description`, `isactive`, `position`, `created_at`, `updated_at`) VALUES
(1, '100', 1, 'Salmon-Ka-Tukra', 1, '5.50', '0', 1, 0, 'Salmon Steak tempered with roasted spices then grilled.', 1, 0, '2018-04-19 12:41:16', '2018-04-19 12:41:16'),
(3, '', 3, 'Bangla Pizza', 1, '0', '0', 3, 0, '<p>Modal Test Size</p>', 1, 0, '2018-04-19 12:46:49', '2018-04-19 12:46:49'),
(4, '', 5, 'Italian Pizza', 1, '5.30', '0', 3, 0, '', 1, 0, '2018-04-19 12:48:27', '2018-04-19 12:48:27'),
(5, '', 2, 'Hydrabadi Biriani', 1, '0', '0', 4, 0, '', 1, 0, '2018-04-19 13:03:51', '2018-04-19 13:03:51'),
(6, '100', 1, 'Panta Ilish', 1, '20.50', '0', 4, 0, '<p>Direct</p>', 1, 0, '2018-04-19 13:06:13', '2018-04-19 13:06:13'),
(7, '100', 1, 'Rice with Beef Acharia', 1, '7.25', '0', 4, 0, NULL, 1, 0, '2018-04-19 13:07:17', '2018-04-19 13:07:17'),
(8, '100', 1, 'Bhuna Khicuri', 1, '5.60', '0', 1, 0, NULL, 1, 0, '2018-04-19 13:10:56', '2018-04-19 13:10:56'),
(9, '100', 1, 'Italiano Pizzao', 1, '7.25', '0', 3, 0, '<p>Extra Cheeze Diret Product<br></p>', 1, 0, '2018-04-19 13:12:23', '2018-04-19 13:12:23'),
(12, '', 4, 'Meal For 2 Persons', 1, '10.25', '7.25', 2, 0, '<p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 13px;\"></span><span class=\" haschillies_0\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 13px;\"></span><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 13px;\"></span></p><div class=\"product_description\" style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 13px;\"><p style=\"padding: 0px 5px; border: 0px; vertical-align: baseline; line-height: 18px; color: rgb(51, 51, 51);\">Worth £34.95 as per menu</p><p style=\"padding: 0px 5px; border: 0px; vertical-align: baseline; line-height: 18px; color: rgb(51, 51, 51);\"><strong style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Appetiser :</strong>&nbsp;2 popadoms | Chutney Set</p><p style=\"padding: 0px 5px; border: 0px; vertical-align: baseline; line-height: 18px; color: rgb(51, 51, 51);\"><strong style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">2 Starter :</strong>&nbsp;Chicken Pakura | Vegetable Pakora</p><p style=\"padding: 0px 5px; border: 0px; vertical-align: baseline; line-height: 18px; color: rgb(51, 51, 51);\"><strong style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">2 Main Dishes :</strong>&nbsp;With Chicken /&nbsp; Vegetable /&nbsp; Lamb or Prawn<br>Kurma* (Mild), Bhuna (Medium), Rogan Josh (Medium), Pathia (Medium to Hot), Madras (Hot), Jalfrezi (Fairly Hot)</p><p style=\"padding: 0px 5px; border: 0px; vertical-align: baseline; line-height: 18px; color: rgb(51, 51, 51);\"><strong style=\"margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\">Two Sundries:&nbsp;</strong>Pilau Rice, Plain Nan</p></div>', 1, 0, '2018-04-22 10:50:24', '2018-04-22 10:50:24'),
(14, '', 3, '1 Man Food Choice', 1, '0', '0', 1, 0, 'Salmon Steak tempered with roasted spices then grilled.', 1, 0, '2018-04-22 12:25:51', '2018-04-22 12:25:51'),
(15, '', 3, '1 man salad', 1, '0', '0', 1, 0, 'Salmon Steak tempered with roasted spices then grilled.', 1, 0, '2018-04-22 12:32:05', '2018-04-22 12:32:05'),
(16, '', 5, 'AFGHANI TIKKA', 1, '6.30', '0', 3, 0, 'Topped with Afghani Tikka chunks and onion on a special creamy pizza sauce.', 1, 0, '2018-04-23 13:41:46', '2018-04-23 13:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `product_colors`
--

CREATE TABLE `product_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_colors`
--

INSERT INTO `product_colors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Red', '2017-02-12 00:19:52', '2017-08-06 13:44:48'),
(2, 'Green', '2017-02-20 20:47:42', '2017-08-06 13:44:53'),
(3, 'Blue', '2017-02-20 20:47:50', '2017-08-06 13:44:59'),
(4, 'White', '2017-08-06 13:45:03', '2017-08-06 13:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `product_one_sub_levels`
--

CREATE TABLE `product_one_sub_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_one_sub_levels`
--

INSERT INTO `product_one_sub_levels` (`id`, `name`, `quantity`, `price`, `pid`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Chicken', 1, '5.95', 9, NULL, '2018-04-14 14:37:07', '2018-04-14 14:37:07'),
(2, 'Lamb', 1, '5.95', 9, NULL, '2018-04-14 14:37:07', '2018-04-14 14:37:07'),
(3, NULL, 1, '0', 10, '0', '2018-04-15 15:28:03', '2018-04-15 15:28:03'),
(4, NULL, 1, '0', 11, '0', '2018-04-15 15:28:45', '2018-04-15 15:28:45'),
(5, 'Appetiser', 1, '0', 11, '0', '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(6, 'Starter', 1, '0', 11, '0', '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(7, 'Extra Cheeze', 1, '2.25', 13, NULL, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(8, 'Extra Beef Curry', 1, '3.50', 13, NULL, '2018-04-19 11:58:21', '2018-04-19 11:58:21'),
(14, 'Large', 1, '20', 16, NULL, '2018-04-19 12:22:17', '2018-04-19 12:22:17'),
(15, 'Medium', 1, '15', 16, NULL, '2018-04-19 12:22:17', '2018-04-19 12:22:17'),
(16, NULL, 1, '10', 21, NULL, '2018-04-19 12:27:34', '2018-04-19 12:27:34'),
(17, 'Platter', 1, '0', 2, '0', '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(18, 'Drinks', 1, '0', 2, '0', '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(19, 'Rice', 1, '0', 2, '0', '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(20, NULL, 1, '10', 3, NULL, '2018-04-19 12:46:49', '2018-04-19 12:46:49'),
(21, 'Extra Beef Curry', 1, '5.30', 4, NULL, '2018-04-19 12:48:28', '2018-04-19 12:48:28'),
(22, 'Extra Cheeze', 1, '2.30', 4, NULL, '2018-04-19 12:48:28', '2018-04-19 12:48:28'),
(23, NULL, 1, '10.25', 5, NULL, '2018-04-19 13:03:51', '2018-04-19 13:03:51'),
(24, 'Appetiser', NULL, NULL, 10, '<', '2018-04-21 14:52:18', '2018-04-21 14:52:18'),
(25, '2 Starter', NULL, NULL, 10, 'p', '2018-04-21 14:52:18', '2018-04-21 14:52:18'),
(26, '2 Main Dishes', NULL, NULL, 10, '>', '2018-04-21 14:52:18', '2018-04-21 14:52:18'),
(27, 'Two Sundries', NULL, NULL, 10, '<', '2018-04-21 14:52:18', '2018-04-21 14:52:18'),
(28, 'Appetiser', NULL, NULL, 11, '2 popadoms,Chutney Set', '2018-04-22 10:48:16', '2018-04-22 10:48:16'),
(29, '2 Starter', NULL, NULL, 11, 'Chicken Pakura,Vegetable Pakora', '2018-04-22 10:48:16', '2018-04-22 10:48:16'),
(30, 'Appetiser', NULL, NULL, 12, '2 popadoms,Chutney Set', '2018-04-22 10:50:24', '2018-04-22 10:50:24'),
(31, '2 Starter', NULL, NULL, 12, 'Chicken Pakura,Vegetable Pakora', '2018-04-22 10:50:24', '2018-04-22 10:50:24'),
(32, 'Two Sundries', NULL, NULL, 12, 'Pilau Rice, Plain Nan', '2018-04-22 10:50:24', '2018-04-22 10:50:24'),
(33, 'Chicket', 1, '10', 14, NULL, '2018-04-22 12:25:51', '2018-04-22 12:25:51'),
(34, 'Beef', 1, '20', 14, NULL, '2018-04-22 12:25:51', '2018-04-22 12:25:51'),
(35, 'Sosha', 1, '10', 15, NULL, '2018-04-22 12:32:06', '2018-04-22 12:32:06'),
(36, 'Gazor', 1, '12', 15, NULL, '2018-04-22 12:32:06', '2018-04-22 12:32:06'),
(37, 'CHOCOLICIOUS BROWNIE', 1, '3.12', 16, 'A rich and decadent treat made with the finest chocolate, perfect ending for your meal.', '2018-04-23 13:41:47', '2018-04-23 13:41:47'),
(38, 'HERSHEY\'S COOKIE', 1, '8.12', 16, '8 Slices of Ooey Gooey Gooey Goodness, Jam-Packed with 100% Genuine Hershey\'s Chocolate Chips.', '2018-04-23 13:41:47', '2018-04-23 13:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `customer_name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_tags`
--

CREATE TABLE `product_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_two_sub_levels`
--

CREATE TABLE `product_two_sub_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `one_pid` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_two_sub_levels`
--

INSERT INTO `product_two_sub_levels` (`id`, `name`, `quantity`, `price`, `pid`, `one_pid`, `description`, `created_at`, `updated_at`) VALUES
(1, '1 POPADOMS', 1, '10', 11, 4, NULL, '2018-04-15 15:28:45', '2018-04-15 15:28:45'),
(2, '2 POPADOMS', 1, '10', 11, 4, NULL, '2018-04-15 15:28:45', '2018-04-15 15:28:45'),
(3, 'Chicken Pakura', 1, '10', 11, 4, NULL, '2018-04-15 15:28:45', '2018-04-15 15:28:45'),
(4, 'Beef Pakura', 1, '10', 11, 4, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(5, '1 POPADOMS', 1, '20', 11, 5, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(6, '2 POPADOMS', 1, '20', 11, 5, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(7, 'Chicken Pakura', 1, '20', 11, 5, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(8, 'Beef Pakura', 1, '20', 11, 5, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(9, '1 POPADOMS', 1, '20', 11, 6, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(10, '2 POPADOMS', 1, '20', 11, 6, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(11, 'Chicken Pakura', 1, '20', 11, 6, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(12, 'Beef Pakura', 1, '20', 11, 6, NULL, '2018-04-15 15:28:46', '2018-04-15 15:28:46'),
(13, '1 Popadoms', 1, '1.75', 14, 9, NULL, '2018-04-19 12:18:35', '2018-04-19 12:18:35'),
(14, '2 Popadoms', 1, '1.75', 14, 9, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(15, 'Chicken', 1, '1.75', 14, 9, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(16, 'Beef', 1, '1.75', 14, 9, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(17, '1 Popadoms', 1, '3.10', 14, 10, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(18, '2 Popadoms', 1, '3.10', 14, 10, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(19, 'Chicken', 1, '3.10', 14, 10, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(20, 'Beef', 1, '3.10', 14, 10, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(21, '1 Popadoms', 1, '5.36', 14, 11, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(22, '2 Popadoms', 1, '5.36', 14, 11, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(23, 'Chicken', 1, '5.36', 14, 11, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(24, 'Beef', 1, '5.36', 14, 11, NULL, '2018-04-19 12:18:36', '2018-04-19 12:18:36'),
(25, NULL, 1, NULL, 2, 17, NULL, '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(26, 'Apatizer', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(27, 'Beef', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(28, 'Pepsi', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(29, 'Seven up', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:29', '2018-04-19 12:45:29'),
(30, 'Indian', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(31, 'Bangla', 1, NULL, 2, 17, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(32, NULL, 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(33, 'Apatizer', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(34, 'Beef', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(35, 'Pepsi', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(36, 'Seven up', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(37, 'Indian', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(38, 'Bangla', 1, '10', 2, 18, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(39, NULL, 1, '20', 2, 19, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(40, 'Apatizer', 1, '20', 2, 19, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(41, 'Beef', 1, '20', 2, 19, NULL, '2018-04-19 12:45:30', '2018-04-19 12:45:30'),
(42, 'Pepsi', 1, '20', 2, 19, NULL, '2018-04-19 12:45:31', '2018-04-19 12:45:31'),
(43, 'Seven up', 1, '20', 2, 19, NULL, '2018-04-19 12:45:31', '2018-04-19 12:45:31'),
(44, 'Indian', 1, '20', 2, 19, NULL, '2018-04-19 12:45:31', '2018-04-19 12:45:31'),
(45, 'Bangla', 1, '20', 2, 19, NULL, '2018-04-19 12:45:31', '2018-04-19 12:45:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_unit_types`
--

CREATE TABLE `product_unit_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_view_by_i_ps`
--

CREATE TABLE `product_view_by_i_ps` (
  `id` int(20) NOT NULL,
  `product_id` int(20) DEFAULT NULL,
  `ip` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `q_r_codes`
--

CREATE TABLE `q_r_codes` (
  `id` int(20) NOT NULL,
  `qrcode` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `q_r_codes`
--

INSERT INTO `q_r_codes` (`id`, `qrcode`, `created_at`, `updated_at`) VALUES
(1, '1507664420.png', '2017-10-10 19:40:20', '2017-10-11 00:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quality` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

CREATE TABLE `seos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seos`
--

INSERT INTO `seos` (`id`, `name`, `site_logo`, `meta`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Ecommerce', '1499534875L.png', 'sdfds', 'sdfdsfsdf', '0000-00-00 00:00:00', '2017-07-08 11:27:55');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shippingimage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `name`, `shippingimage`, `description`, `price`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'UPS', '1491856896.png', 'asdsadsadsad', 10, 1, '2017-04-10 14:41:36', '2017-07-22 12:28:22'),
(2, 'DHL', '1491857086.png', 'sdsadsa', 15, 0, '2017-04-10 14:44:46', '2017-04-10 14:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sliderimage` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `product_id`, `name`, `sliderimage`, `description`, `isactive`, `created_at`, `updated_at`) VALUES
(2, 50, 'Hi choice', '1506018773.jpg', 'kkkk', 1, '2017-03-11 14:42:19', '2017-09-21 23:32:53'),
(3, 0, 'Test & Coaching', '1489264950.jpg', 'xZXzX', 0, '2017-03-11 14:42:30', '2017-07-10 11:52:18'),
(4, 0, 'Demo', '1489264961.jpg', 'ZxxZX', 0, '2017-03-11 14:42:41', '2017-07-10 11:51:47');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe_loads`
--

CREATE TABLE `subscribe_loads` (
  `id` int(20) NOT NULL,
  `pc_name` text,
  `pc_ip` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe_loads`
--

INSERT INTO `subscribe_loads` (`id`, `pc_name`, `pc_ip`, `created_at`, `updated_at`) VALUES
(3, '0', '103.91.144.20', '2017-09-16 23:54:35', '0000-00-00 00:00:00'),
(4, 'nuc.nucleuspos.com', '103.91.144.20', '2017-09-16 23:57:26', '0000-00-00 00:00:00'),
(5, '103.91.144.20', '103.91.144.20', '2017-09-16 23:59:12', '0000-00-00 00:00:00'),
(6, 'AWBL33-103.qubee.com.bd', '180.234.33.103', '2017-09-17 00:20:58', '0000-00-00 00:00:00'),
(7, 'AWBL55-143.qubee.com.bd', '180.234.55.143', '2017-09-17 18:23:59', '0000-00-00 00:00:00'),
(8, 'google-proxy-66-249-82-157.google.com', '66.249.82.157', '2017-09-17 18:27:03', '0000-00-00 00:00:00'),
(9, 'AWBL89-192.qubee.com.bd', '180.234.89.192', '2017-09-17 19:56:30', '0000-00-00 00:00:00'),
(10, 'AWBL89-142.qubee.com.bd', '180.234.89.142', '2017-09-17 21:00:16', '0000-00-00 00:00:00'),
(11, 'dr-steve.w3.org', '128.30.52.97', '2017-09-18 01:02:06', '0000-00-00 00:00:00'),
(12, 'AWBL95-39.qubee.com.bd', '180.234.95.39', '2017-09-18 03:23:11', '0000-00-00 00:00:00'),
(13, '119.30.45.128', '119.30.45.128', '2017-09-18 09:31:36', '0000-00-00 00:00:00'),
(14, 'AWBL33-197.qubee.com.bd', '180.234.33.197', '2017-09-18 14:28:11', '0000-00-00 00:00:00'),
(15, '123.108.244.56', '123.108.244.56', '2017-09-18 14:34:05', '0000-00-00 00:00:00'),
(16, '37.111.233.210', '37.111.233.210', '2017-09-18 19:13:43', '0000-00-00 00:00:00'),
(17, 'AWBL37-132.qubee.com.bd', '180.234.37.132', '2017-09-19 01:50:18', '0000-00-00 00:00:00'),
(18, 'AWBL31-161.qubee.com.bd', '180.234.31.161', '2017-09-19 16:51:24', '0000-00-00 00:00:00'),
(19, 'AWBL95-194.qubee.com.bd', '180.234.95.194', '2017-09-19 19:53:28', '0000-00-00 00:00:00'),
(20, 'AWBL68-172.qubee.com.bd', '180.234.68.172', '2017-09-19 20:03:36', '0000-00-00 00:00:00'),
(21, 'AWBL68-18.qubee.com.bd', '180.234.68.18', '2017-09-19 20:50:43', '0000-00-00 00:00:00'),
(22, 'AWBL59-174.qubee.com.bd', '180.234.59.174', '2017-09-19 22:59:15', '0000-00-00 00:00:00'),
(23, 'AWBL68-7.qubee.com.bd', '180.234.68.7', '2017-09-20 13:35:06', '0000-00-00 00:00:00'),
(24, 'AWBL55-157.qubee.com.bd', '180.234.55.157', '2017-09-20 14:57:31', '0000-00-00 00:00:00'),
(25, '13.76.241.210', '13.76.241.210', '2017-09-20 16:29:47', '0000-00-00 00:00:00'),
(26, 'AWBL59-53.qubee.com.bd', '180.234.59.53', '2017-09-20 17:59:21', '0000-00-00 00:00:00'),
(27, 'AWBL55-138.qubee.com.bd', '180.234.55.138', '2017-09-21 00:31:54', '0000-00-00 00:00:00'),
(28, 'metrix-na-west6.nmsrv.com', '204.187.14.75', '2017-09-21 00:40:04', '0000-00-00 00:00:00'),
(29, 'metrix-na-west1.nmsrv.com', '204.187.14.70', '2017-09-21 00:44:49', '0000-00-00 00:00:00'),
(30, 'metrix-na-west8.nmsrv.com', '204.187.14.77', '2017-09-21 00:55:17', '0000-00-00 00:00:00'),
(31, '104.209.188.207', '104.209.188.207', '2017-09-21 23:15:31', '0000-00-00 00:00:00'),
(32, '104.45.18.178', '104.45.18.178', '2017-09-21 23:15:33', '0000-00-00 00:00:00'),
(33, 'AWBL83-132.qubee.com.bd', '180.234.83.132', '2017-09-21 23:15:41', '0000-00-00 00:00:00'),
(34, 'AWBL95-231.qubee.com.bd', '180.234.95.231', '2017-09-21 23:43:29', '0000-00-00 00:00:00'),
(35, 'AWBL76-184.qubee.com.bd', '180.234.76.184', '2017-09-22 00:50:22', '0000-00-00 00:00:00'),
(36, '23.99.101.118', '23.99.101.118', '2017-09-22 01:57:14', '0000-00-00 00:00:00'),
(37, 'AWBL68-99.qubee.com.bd', '180.234.68.99', '2017-09-22 01:57:28', '0000-00-00 00:00:00'),
(38, 'AWBL33-120.qubee.com.bd', '180.234.33.120', '2017-09-22 13:04:46', '0000-00-00 00:00:00'),
(39, 'AWBL36-178.qubee.com.bd', '180.234.36.178', '2017-09-22 13:59:05', '0000-00-00 00:00:00'),
(40, 'AWBL37-193.qubee.com.bd', '180.234.37.193', '2017-09-22 16:17:53', '0000-00-00 00:00:00'),
(41, 'AWBL95-30.qubee.com.bd', '180.234.95.30', '2017-09-23 16:43:20', '0000-00-00 00:00:00'),
(42, 'AWBL37-174.qubee.com.bd', '180.234.37.174', '2017-09-23 21:08:16', '0000-00-00 00:00:00'),
(43, 'AWBL33-228.qubee.com.bd', '180.234.33.228', '2017-09-24 01:52:41', '0000-00-00 00:00:00'),
(44, 'google-proxy-64-233-172-185.google.com', '64.233.172.185', '2017-09-24 21:27:25', '0000-00-00 00:00:00'),
(45, 'google-proxy-64-233-173-11.google.com', '64.233.173.11', '2017-09-24 21:45:28', '0000-00-00 00:00:00'),
(46, 'AWBL89-219.qubee.com.bd', '180.234.89.219', '2017-09-24 21:46:50', '0000-00-00 00:00:00'),
(47, 'AWBL33-248.qubee.com.bd', '180.234.33.248', '2017-09-25 14:02:06', '0000-00-00 00:00:00'),
(48, 'AWBL55-117.qubee.com.bd', '180.234.55.117', '2017-09-26 13:29:44', '0000-00-00 00:00:00'),
(49, 'google-proxy-64-233-172-187.google.com', '64.233.172.187', '2017-09-27 23:21:29', '0000-00-00 00:00:00'),
(50, 'google-proxy-66-249-82-158.google.com', '66.249.82.158', '2017-09-28 14:26:20', '0000-00-00 00:00:00'),
(51, 'AWBL95-241.qubee.com.bd', '180.234.95.241', '2017-09-30 03:13:58', '0000-00-00 00:00:00'),
(52, '162.243.69.215', '162.243.69.215', '2017-09-30 03:14:08', '0000-00-00 00:00:00'),
(53, 'AWBL36-97.qubee.com.bd', '180.234.36.97', '2017-09-30 16:09:04', '0000-00-00 00:00:00'),
(54, 'AWBL95-242.qubee.com.bd', '180.234.95.242', '2017-09-30 22:56:23', '0000-00-00 00:00:00'),
(55, 'AWBL41-234.qubee.com.bd', '180.234.41.234', '2017-10-01 10:41:13', '0000-00-00 00:00:00'),
(56, 'AWBL31-48.qubee.com.bd', '180.234.31.48', '2017-10-02 00:21:48', '0000-00-00 00:00:00'),
(57, 'AWBL83-7.qubee.com.bd', '180.234.83.7', '2017-10-02 13:57:06', '0000-00-00 00:00:00'),
(58, 'google-proxy-66-249-82-145.google.com', '66.249.82.145', '2017-10-03 22:07:08', '0000-00-00 00:00:00'),
(59, 'AWBL59-111.qubee.com.bd', '180.234.59.111', '2017-10-04 22:28:06', '0000-00-00 00:00:00'),
(60, 'AWBL33-42.qubee.com.bd', '180.234.33.42', '2017-10-05 01:11:30', '0000-00-00 00:00:00'),
(61, '79-77-199-51.dynamic.dsl.as9105.com', '79.77.199.51', '2017-10-05 13:45:17', '0000-00-00 00:00:00'),
(62, 'AWBL89-100.qubee.com.bd', '180.234.89.100', '2017-10-05 16:08:56', '0000-00-00 00:00:00'),
(63, 'AWBL55-153.qubee.com.bd', '180.234.55.153', '2017-10-06 23:22:59', '0000-00-00 00:00:00'),
(64, 'AWBL89-147.qubee.com.bd', '180.234.89.147', '2017-10-07 12:34:15', '0000-00-00 00:00:00'),
(65, 'AWBL95-40.qubee.com.bd', '180.234.95.40', '2017-10-07 23:22:53', '0000-00-00 00:00:00'),
(66, 'host-92-20-204-51.as13285.net', '92.20.204.51', '2017-10-09 14:12:38', '0000-00-00 00:00:00'),
(67, 'AWBL83-229.qubee.com.bd', '180.234.83.229', '2017-10-09 14:27:36', '0000-00-00 00:00:00'),
(68, 'AWBL71-250.qubee.com.bd', '180.234.71.250', '2017-10-09 21:05:47', '0000-00-00 00:00:00'),
(69, 'google-proxy-66-249-82-156.google.com', '66.249.82.156', '2017-10-09 21:19:43', '0000-00-00 00:00:00'),
(70, 'AWBL37-43.qubee.com.bd', '180.234.37.43', '2017-10-10 12:18:28', '0000-00-00 00:00:00'),
(71, '79-77-193-184.dynamic.dsl.as9105.com', '79.77.193.184', '2017-10-10 14:58:06', '0000-00-00 00:00:00'),
(72, 'AWBL22-234.qubee.com.bd', '180.234.22.234', '2017-10-11 17:16:18', '0000-00-00 00:00:00'),
(73, 'AWBL71-193.qubee.com.bd', '180.234.71.193', '2017-10-12 23:53:32', '0000-00-00 00:00:00'),
(74, 'AWBL36-83.qubee.com.bd', '180.234.36.83', '2017-10-13 14:13:23', '0000-00-00 00:00:00'),
(75, '79-77-192-185.dynamic.dsl.as9105.com', '79.77.192.185', '2017-10-13 14:28:28', '0000-00-00 00:00:00'),
(76, 'AWBL55-6.qubee.com.bd', '180.234.55.6', '2017-10-15 20:19:21', '0000-00-00 00:00:00'),
(77, 'google-proxy-66-102-6-27.google.com', '66.102.6.27', '2017-10-16 13:13:39', '0000-00-00 00:00:00'),
(78, 'google-proxy-66-249-80-59.google.com', '66.249.80.59', '2017-10-16 13:13:39', '0000-00-00 00:00:00'),
(79, 'AWBL37-223.qubee.com.bd', '180.234.37.223', '2017-10-16 13:14:08', '0000-00-00 00:00:00'),
(80, 'AWBL36-72.qubee.com.bd', '180.234.36.72', '2017-10-16 22:49:11', '0000-00-00 00:00:00'),
(81, 'AWBL59-209.qubee.com.bd', '180.234.59.209', '2017-10-17 18:30:52', '0000-00-00 00:00:00'),
(82, '119.30.32.84', '119.30.32.84', '2017-10-17 20:14:22', '0000-00-00 00:00:00'),
(83, 'AWBL33-40.qubee.com.bd', '180.234.33.40', '2017-10-18 11:09:27', '0000-00-00 00:00:00'),
(84, 'AWBL49-251.qubee.com.bd', '180.234.49.251', '2017-10-18 23:35:22', '0000-00-00 00:00:00'),
(85, '119.30.39.215', '119.30.39.215', '2017-10-19 11:44:17', '0000-00-00 00:00:00'),
(86, 'AWBL37-74.qubee.com.bd', '180.234.37.74', '2017-10-19 19:42:15', '0000-00-00 00:00:00'),
(87, 'AWBL33-99.qubee.com.bd', '180.234.33.99', '2017-10-20 19:22:11', '0000-00-00 00:00:00'),
(88, 'AWBL36-244.qubee.com.bd', '180.234.36.244', '2017-10-21 12:18:53', '0000-00-00 00:00:00'),
(89, 'AWBL37-69.qubee.com.bd', '180.234.37.69', '2017-10-21 20:24:32', '0000-00-00 00:00:00'),
(90, 'AWBL37-15.qubee.com.bd', '180.234.37.15', '2017-10-22 18:20:33', '0000-00-00 00:00:00'),
(91, 'AWBL76-73.qubee.com.bd', '180.234.76.73', '2017-10-22 22:16:33', '0000-00-00 00:00:00'),
(92, 'AWBL76-93.qubee.com.bd', '180.234.76.93', '2017-10-23 10:36:28', '0000-00-00 00:00:00'),
(93, 'AWBL83-228.qubee.com.bd', '180.234.83.228', '2017-10-23 18:00:22', '0000-00-00 00:00:00'),
(94, 'AWBL68-143.qubee.com.bd', '180.234.68.143', '2017-10-24 19:31:39', '0000-00-00 00:00:00'),
(95, 'AWBL68-85.qubee.com.bd', '180.234.68.85', '2017-10-24 22:59:39', '0000-00-00 00:00:00'),
(96, 'AWBL22-231.qubee.com.bd', '180.234.22.231', '2017-10-25 18:46:42', '0000-00-00 00:00:00'),
(97, 'AWBL59-30.qubee.com.bd', '180.234.59.30', '2017-10-26 14:16:06', '0000-00-00 00:00:00'),
(98, 'AWBL59-63.qubee.com.bd', '180.234.59.63', '2017-10-26 18:13:10', '0000-00-00 00:00:00'),
(99, 'AWBL55-189.qubee.com.bd', '180.234.55.189', '2017-10-27 14:54:26', '0000-00-00 00:00:00'),
(100, 'AWBL33-76.qubee.com.bd', '180.234.33.76', '2017-10-27 18:52:10', '0000-00-00 00:00:00'),
(101, 'AWBL22-94.qubee.com.bd', '180.234.22.94', '2017-10-28 14:23:38', '0000-00-00 00:00:00'),
(102, 'AWBL71-240.qubee.com.bd', '180.234.71.240', '2017-10-28 23:11:46', '0000-00-00 00:00:00'),
(103, 'AWBL83-57.qubee.com.bd', '180.234.83.57', '2017-10-29 14:19:49', '0000-00-00 00:00:00'),
(104, 'AWBL68-78.qubee.com.bd', '180.234.68.78', '2017-10-29 19:58:48', '0000-00-00 00:00:00'),
(105, 'AWBL36-189.qubee.com.bd', '180.234.36.189', '2017-10-30 20:42:20', '0000-00-00 00:00:00'),
(106, 'AWBL31-63.qubee.com.bd', '180.234.31.63', '2017-10-31 20:49:20', '0000-00-00 00:00:00'),
(107, 'AWBL22-240.qubee.com.bd', '180.234.22.240', '2017-11-01 08:57:10', '0000-00-00 00:00:00'),
(108, 'AWBL33-101.qubee.com.bd', '180.234.33.101', '2017-11-02 23:39:06', '0000-00-00 00:00:00'),
(109, 'AWBL59-213.qubee.com.bd', '180.234.59.213', '2017-11-03 13:21:32', '0000-00-00 00:00:00'),
(110, 'google-proxy-64-233-173-10.google.com', '64.233.173.10', '2017-11-03 19:40:46', '0000-00-00 00:00:00'),
(111, 'AWBL95-178.qubee.com.bd', '180.234.95.178', '2017-11-03 19:44:28', '0000-00-00 00:00:00'),
(112, 'AWBL55-210.qubee.com.bd', '180.234.55.210', '2017-11-04 00:50:22', '0000-00-00 00:00:00'),
(113, 'host-92-21-33-127.as13285.net', '92.21.33.127', '2017-11-04 15:57:26', '0000-00-00 00:00:00'),
(114, 'msnbot-157-55-39-102.search.msn.com', '157.55.39.102', '2017-11-04 20:18:03', '0000-00-00 00:00:00'),
(115, 'AWBL76-199.qubee.com.bd', '180.234.76.199', '2017-11-05 00:34:24', '0000-00-00 00:00:00'),
(116, 'AWBL49-34.qubee.com.bd', '180.234.49.34', '2017-11-08 05:06:39', '0000-00-00 00:00:00'),
(117, 'AWBL83-54.qubee.com.bd', '180.234.83.54', '2017-11-08 15:56:06', '0000-00-00 00:00:00'),
(118, '59.152.103.230', '59.152.103.230', '2017-11-09 15:55:59', '0000-00-00 00:00:00'),
(119, 'google-proxy-66-102-6-187.google.com', '66.102.6.187', '2017-11-10 22:45:00', '0000-00-00 00:00:00'),
(120, 'google-proxy-66-102-6-77.google.com', '66.102.6.77', '2017-11-11 20:36:33', '0000-00-00 00:00:00'),
(121, 'AWBL89-39.qubee.com.bd', '180.234.89.39', '2017-11-12 15:30:58', '0000-00-00 00:00:00'),
(122, 'google-proxy-66-249-83-87.google.com', '66.249.83.87', '2017-11-12 19:52:06', '0000-00-00 00:00:00'),
(123, 'google-proxy-66-102-6-73.google.com', '66.102.6.73', '2017-11-16 04:16:19', '0000-00-00 00:00:00'),
(124, 'AWBL76-215.qubee.com.bd', '180.234.76.215', '2017-11-16 14:53:36', '0000-00-00 00:00:00'),
(125, 'AWBL55-99.qubee.com.bd', '180.234.55.99', '2017-11-17 00:53:49', '0000-00-00 00:00:00'),
(126, 'google-proxy-66-249-83-223.google.com', '66.249.83.223', '2017-11-18 17:20:44', '0000-00-00 00:00:00'),
(127, 'google-proxy-66-102-6-203.google.com', '66.102.6.203', '2017-11-22 00:29:20', '0000-00-00 00:00:00'),
(128, 'AWBL55-140.qubee.com.bd', '180.234.55.140', '2017-11-22 01:31:15', '0000-00-00 00:00:00'),
(129, '45.114.86.69', '45.114.86.69', '2017-11-25 14:03:25', '0000-00-00 00:00:00'),
(130, 'AWBL68-158.qubee.com.bd', '180.234.68.158', '2017-11-25 18:37:22', '0000-00-00 00:00:00'),
(131, 'AWBL37-57.qubee.com.bd', '180.234.37.57', '2017-12-01 12:07:26', '0000-00-00 00:00:00'),
(132, 'AWBL41-224.qubee.com.bd', '180.234.41.224', '2017-12-01 16:11:27', '0000-00-00 00:00:00'),
(133, 'AWBL89-177.qubee.com.bd', '180.234.89.177', '2017-12-10 02:08:33', '0000-00-00 00:00:00'),
(134, '103.91.144.43', '103.91.144.43', '2017-12-12 02:09:58', '0000-00-00 00:00:00'),
(135, 'AWBL41-28.qubee.com.bd', '180.234.41.28', '2017-12-17 01:21:42', '0000-00-00 00:00:00'),
(136, 'DESKTOP-NVJV7AA', '::1', '2017-12-31 14:22:16', '0000-00-00 00:00:00'),
(137, 'DESKTOP-PHFRTLE', '192.168.0.10', '2018-02-09 14:56:25', '0000-00-00 00:00:00'),
(138, 'ROOTMAP-PC', '192.168.0.3', '2018-03-13 13:00:54', '0000-00-00 00:00:00'),
(139, 'ROOTMAP-PC', '192.168.0.5', '2018-03-17 10:31:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(2, 17, 'KORMA', 'Mild', '2018-04-03 12:54:34', '2018-04-03 12:54:34'),
(3, 17, 'CURRY', '(Medium)', '2018-04-03 12:55:01', '2018-04-03 12:55:01'),
(4, 17, 'Bhuna', '(Medium)', '2018-04-03 13:06:47', '2018-04-03 13:06:47'),
(5, 17, 'Rogan Josh', '(Medium)', '2018-04-03 13:07:02', '2018-04-03 13:07:02'),
(6, 17, 'Dupiaza', '(Medium)', '2018-04-03 13:07:24', '2018-04-03 13:07:24'),
(7, 17, 'Dhansak', '(Medium)', '2018-04-03 13:07:51', '2018-04-03 13:07:51'),
(8, 17, 'Madras', '(Fairly Hot)', '2018-04-03 13:08:05', '2018-04-03 13:08:05'),
(9, 17, 'Samber', '(Fairly Hot)', '2018-04-03 13:08:28', '2018-04-03 13:08:28'),
(10, 17, 'Pathia', '(Fairly Hot)', '2018-04-03 13:08:44', '2018-04-03 13:08:44'),
(11, 17, 'Ceylon', '(Fairly Hot)', '2018-04-03 13:09:09', '2018-04-03 13:09:09'),
(12, 17, 'Vindaloo', '(Hot)', '2018-04-03 13:09:37', '2018-04-03 13:09:37'),
(13, 18, 'TANDOORI TOOFAN', 'HOT) a mouth storming dish cooked with hot spices and pickled chillies', '2018-04-03 13:13:10', '2018-04-03 13:13:10'),
(14, 18, 'HYDERABADI', '(MEDIUM) marinated meat engulfed in pickles and spices hyrabadi style - tasty!', '2018-04-03 13:13:32', '2018-04-03 13:13:32'),
(15, 4, 'Indian', 'Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here.', '2018-04-19 12:57:48', '2018-04-19 12:57:48'),
(16, 4, 'Bangla', 'Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here Demo Text Here', '2018-04-19 12:58:06', '2018-04-19 12:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `category_id`, `sub_category_id`, `name`, `photo`, `description`, `created_at`, `updated_at`) VALUES
(1, 6, 65, 'Signature', '1511293633S.jpg', 'Signature', '2017-11-22 01:47:13', '2018-01-07 14:04:49'),
(2, 6, 66, 'Tableware', '1515355549S.jpg', 'fddsfd', '2018-01-07 14:05:49', '2018-01-07 14:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `table_bookings`
--

CREATE TABLE `table_bookings` (
  `id` int(11) NOT NULL,
  `fullname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_of_person` int(20) NOT NULL,
  `reservation_date` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reservation_time` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_bookings`
--

INSERT INTO `table_bookings` (`id`, `fullname`, `email`, `phone`, `number_of_person`, `reservation_date`, `reservation_time`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Fakhrul Islam', 'fakhrul.dia@gmail.com', '01677136045', 2, '4/3/2018', '4:55 PM', 'asdsadasdasdasdasdasdas', '2018-04-03 10:55:31', '2018-04-03 04:55:31');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Glossary', '2017-02-12 06:19:52', '2017-03-26 03:12:00'),
(2, 'CM', '2017-02-21 02:47:42', '2017-08-06 13:33:56'),
(3, 'Cloths', '2017-02-21 02:47:50', '2017-02-21 02:47:50'),
(4, 'Ton', '2017-08-06 13:33:46', '2017-08-06 13:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `today_offers`
--

CREATE TABLE `today_offers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `offerimage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isactive` int(2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `today_offers`
--

INSERT INTO `today_offers` (`id`, `title`, `name`, `description`, `offerimage`, `isactive`, `created_at`, `updated_at`) VALUES
(1, 'Kendo UI - Add / Remove with kendo.data.DataSource', 'CHEF’S SPECIAL DISHES', 'caccacacacacaca', '1522756288.JPG', 0, '2018-04-03 11:51:28', '2018-04-03 05:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `paid` double(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('Customer','Admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `user_type`, `created_at`, `updated_at`) VALUES
(3, 'Fahad Bhuyian', 'fahadvampare@gmail.com', '$2y$10$.88YPT8NZklZKKmZU2gWLOLMha1GkpXRyeuVQtLoWxSRV5jwazfDG', 'kzfXB8OtDHrecACqImfh4jxrgTdBvhJyxGECRxpziKdbIfSFF7abSdiXuV94', 'Customer', '2017-05-01 03:29:08', '2017-05-01 03:29:08'),
(4, 'Fakhrul', 'fakhrul@gmail.com', '$2y$10$T.is8bwV7l354nHPCZgBlOePOYHSuaWempPVzhQgY/oJpyi6Tmnf2', '2XSTlhndcgvBuSwQvHSEVBmDSPcSN8Bn0g7PXQHAPxOt9a3nS9KD4AAjly0p', 'Customer', '2017-05-01 03:32:20', '2017-05-01 03:32:20'),
(5, 'Munira', 'munira@gmail.com', '$2y$10$g8CM6brmGKy.fs6LsPpJneA7Rv2Fvw50Wwp0M9FgAI6uMj6eMypxy', NULL, 'Customer', '2017-05-01 03:33:31', '2017-05-01 03:33:31'),
(6, 'Demo', 'demo@gmail.com', '$2y$10$Nbc/EC2ujIep.UXa5KpZcuhTyDnBYX29qeifev2xkA1l6bIIJAAOu', NULL, 'Customer', '2017-05-01 03:34:20', '2017-05-01 03:34:20'),
(7, 'Fahad', 'fahad@gmail.com', '$2y$10$Ou65eimeIv2UZ4DVuGVP5eECS48KLYufkipBd521TlwiJtGChVgre', '1JDq4KqqM0zRs8bu7V9oXQvyI5mBZPGEH0iAbQldfhwOqkzn7AJbNXjIFG5C', 'Customer', '2017-06-02 13:00:19', '2017-06-02 13:00:19'),
(8, 'Fakhrul', 'fkhrl@gmail.com', '$2y$10$MNv3iupeJp7S6T/Z.RZ0b.Q8C.bKpw10.T7llf.nADmvbe5l1Moz.', NULL, 'Customer', '2017-06-07 03:13:44', '2017-06-07 03:13:44'),
(9, 'rony', 'rony@gmail.com', '$2y$10$QozSdc2WW3T.WUj8ug8JQeCLWuRgJIZYZPXMqNCxBosJUqnF5wBue', 'cvadJi7MtTYd6ptN0u52D9ELmg330RyPkcqdCmYNEP0Y2Eua6OE0kxLSUFxY', 'Customer', '2017-06-07 13:25:31', '2017-06-07 13:25:31'),
(10, 'RENT-A-CAR SERVICE', 'r@gmail.com', '$2y$10$D7tTYVYP.NCooes2370Hr.NWp.mF/2sJKEh3Xo4sh38sUbEnCPdhO', NULL, 'Customer', '2017-06-07 13:49:40', '2017-06-07 13:49:40'),
(11, 'Fakhrul', 'fakhrul606399@gmail.com', '$2y$10$CAMYBzbXOs7nyZiZuU9/ceZ1y4wct26L1b20PqBCf8lJEISwm8W8m', 'SKaYcdXOB4ZgpDdGmhZmAnFMGfIDtrCDifcbVfsj7vuzuXQ3ZLft0gpX92je', 'Customer', '2017-07-10 01:23:27', '2017-07-10 01:23:27'),
(26, 'Fahad Bhuy', 'f.bhuyian@gmail.com', '$2y$10$ghnsh3UjicM8IsSCMLn0M.NdvxnuYmLNe3.EEBF2HwtRQ.gqMn6.O', 'JcxoYb8zYXvyTnSGpZBAD6R2xiH9CSaGQxaqEuvU85DuF9DQ6fyKh84RFG9B', 'Admin', '2017-07-16 13:38:16', '2017-07-16 13:38:16'),
(27, 'dfdsfds', 'fahad@systechunimax.com', '$2y$10$GcJydOTm/Y1dkzEDveRDm.nsXxkEuRXP3jRjFxV3.URSflZ68Je7e', 'PAEzjfiGn3OXmybuXahIAlZN5SQfE9hKDtL5Q1DPU6VBFp5mI9YnREb1Ezbt', 'Customer', '2017-07-16 13:40:47', '2017-07-16 13:40:47'),
(28, 'Parash', 'fakhrul_islam21@diit.info', '$2y$10$mw/bEUQrmVcVUK/le4f2buKJuvSn6Uovcx3oX0im8GRHwyS16ow46', NULL, 'Customer', '2017-07-21 10:46:39', '2017-07-21 10:46:39'),
(29, 'shaiful islam', 'shaiful1408@gmail.com', '$2y$10$uTfnkNKyuoJM6gQdpRkx9ONdCozxa229ZNArn4J35u9.r63NNwcqi', 'j6QmrEGJCzjTGLymcdhwBRXemsZkDQli5lMF1i9BqNpQLa2FmRSqiO5RMm55', 'Customer', '2017-09-12 00:03:33', '2017-09-12 00:03:33'),
(30, 'Kamal hemel', 'kamalhemel@gmail.com', '$2y$10$yEDEzpgrNZbG/558V5F/Be1rsYwho2HUJvblbjY1jY0Z9j/Oa970W', 'wOWxXroMH2C6tY9x2oWDwKRL4hmuDpOTr2GnBhVyf6KSwWBZpOkFVmesY344', 'Customer', '2017-09-19 19:51:53', '2017-09-19 19:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_in_products`
--
ALTER TABLE `color_in_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_details`
--
ALTER TABLE `contact_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_pages`
--
ALTER TABLE `contact_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_requests`
--
ALTER TABLE `contact_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_supports`
--
ALTER TABLE `customer_supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_addresses_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `invoice_addresses`
--
ALTER TABLE `invoice_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordersdeliverymethods`
--
ALTER TABLE `ordersdeliverymethods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_items`
--
ALTER TABLE `orders_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_emails`
--
ALTER TABLE `order_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_infos`
--
ALTER TABLE `order_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payment_methods`
--
ALTER TABLE `order_payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pizza_flabours`
--
ALTER TABLE `pizza_flabours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pizza_sizes`
--
ALTER TABLE `pizza_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_one_sub_levels`
--
ALTER TABLE `product_one_sub_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_tags`
--
ALTER TABLE `product_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_two_sub_levels`
--
ALTER TABLE `product_two_sub_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_unit_types`
--
ALTER TABLE `product_unit_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_view_by_i_ps`
--
ALTER TABLE `product_view_by_i_ps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `q_r_codes`
--
ALTER TABLE `q_r_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `seos`
--
ALTER TABLE `seos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribe_loads`
--
ALTER TABLE `subscribe_loads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_bookings`
--
ALTER TABLE `table_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `today_offers`
--
ALTER TABLE `today_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `color_in_products`
--
ALTER TABLE `color_in_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_details`
--
ALTER TABLE `contact_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_pages`
--
ALTER TABLE `contact_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contact_requests`
--
ALTER TABLE `contact_requests`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customer_supports`
--
ALTER TABLE `customer_supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `invoice_addresses`
--
ALTER TABLE `invoice_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `ordersdeliverymethods`
--
ALTER TABLE `ordersdeliverymethods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `orders_items`
--
ALTER TABLE `orders_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `order_emails`
--
ALTER TABLE `order_emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_infos`
--
ALTER TABLE `order_infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `order_payment_methods`
--
ALTER TABLE `order_payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `order_transactions`
--
ALTER TABLE `order_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pizza_flabours`
--
ALTER TABLE `pizza_flabours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pizza_sizes`
--
ALTER TABLE `pizza_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_one_sub_levels`
--
ALTER TABLE `product_one_sub_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_tags`
--
ALTER TABLE `product_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_two_sub_levels`
--
ALTER TABLE `product_two_sub_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `product_unit_types`
--
ALTER TABLE `product_unit_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_view_by_i_ps`
--
ALTER TABLE `product_view_by_i_ps`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `q_r_codes`
--
ALTER TABLE `q_r_codes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seos`
--
ALTER TABLE `seos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscribe_loads`
--
ALTER TABLE `subscribe_loads`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_bookings`
--
ALTER TABLE `table_bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `today_offers`
--
ALTER TABLE `today_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
