<?php 
    $objSTD=new MenuPageController();
    $customerSupport=$objSTD->CustomerSupport();
    $ContactDetail=$objSTD->ContactDetail();
    $Seo=$objSTD->Seo();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>@yield('title') | thebombaycottage.com</title>
        <meta name="description" content="Selfcode.space – Restouent System">
        <meta name="author" content="Md. Mahamudur Zaman Bhuyian">

        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        @if(Auth::check())
            @if(Auth::user()->user_type=='Admin')
                <script type="text/javascript">
                    window.location = "{{url('admin-ecom/dashboard')}}";//here double curly bracket
                </script>
                <?php exit(); ?>
            @endif
        @endif
        <!-- Put favicon.ico and apple-touch-icon(s).png in the images folder -->
        @include('frontend.extra.header-css')
        <!--[if lt IE 9]>
        <link rel="stylesheet" href="css/ie.css">
        <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->

    	@yield('css')

        
    </head>
    <body>

        <!-- site preloader start -->
        <div class="page-loader">
            <div class="loader-in"></div>
        </div>
        <!-- site preloader end -->

        <div class="pageWrapper">
            <!-- login box start -->
            <?php //include_once './include/login.php'; ?>
            <!-- login box End -->

            <!-- Header Start -->
            <div id="headWrapper" class="clearfix">

                <!-- top bar start -->
                @include('frontend.extra.top_bar')
                <!-- top bar end -->

                @include('frontend.extra.logo_menu')
            </div>
            <!-- Header End -->

            <!-- Content Start -->
            @yield('content')
            <!-- Content End -->
            <!-- Footer start -->
            @include('frontend.extra.fotter')
            <!-- Footer end -->
        </div> 
        @include('frontend.extra.fotter-script')
        @yield('js')
        <script type="text/javascript">
            $(document).ready(function(){
                $(".close-messagelbl-area").click(function(){
                    $(".lbl-box-message-area").fadeOut('slow');
                });

                $(".logoutFront").click(function(){
                    $("#logoutFront").submit();
                });
                
            });
        </script>
    </body>
</html>