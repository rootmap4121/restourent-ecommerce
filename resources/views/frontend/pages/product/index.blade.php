@extends('frontend.layout.master')
@section('title','Order Your Menu')
@section('content')	
   <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="cell-12">
                    <h1 class="fx proban" data-animate="fadeInLeft">Polo T-Shirt For Men And Women</h1>
                    <div class="breadcrumbs main-bg fx" data-animate="fadeInUp">
                        <span class="bold">You Are In:</span><a href="#">Home</a><span class="line-separate">/</span><a href="#">Pages </a><span class="line-separate">/</span><a href="#">Shop </a><span class="line-separate">/</span><span>Polo T-Shirt For Men</span>
                        
                    </div>
                    <div class="my-sticky-element" data-sticky="true">
                        <div class="cusButonRight">
                                <a class="btn btn-md btn-square main-bg fx animated fadeInUp" href="login1.php" data-animate="fadeInUp" data-animation-delay="100" style="animation-delay: 100ms;">
                                <span><i class="fa fa-crop selectedI"></i>login</span>
                                </a>
                                <a class="btn btn-md btn-square main-bg fx animated fadeInUp" href="registation.php" data-animate="fadeInUp" data-animation-delay="100" style="animation-delay: 100ms;">
                                    <span><i class="fa fa-crop selectedI"></i>REGISTER</span>
                                </a>
                            </div>
                        <div class="cart-icon fx" data-animate="fadeInRight">
                            <div class="cart-heading">
                                <i class="fa fa-shopping-cart"></i><span id="cart-count"><b>0</b> item(s)</span>
                            </div>
                            <div class="cart-popup">
                                <div class="empty">Your shopping cart is empty!</div>
                                <div class="mini-cart">
                                    <ul class="mini-cart-list">
                                        <li>
                                            <a class="cart-mini-lft" href="product.html"><img src="{{url('front-theme/images/shop/pro-1.jpg')}}" alt=""></a>
                                            <div class="cart-body">
                                                <a href="product.html">Ultimate Fashion Wear White</a>
                                                <div class="price">$150</div>
                                            </div>
                                            <a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
                                        </li>
                                        <li>
                                            <a class="cart-mini-lft" href="product.html"><img src="{{url('front-theme/images/shop/pro-2.jpg')}}" alt=""></a>
                                            <div class="cart-body">
                                                <a href="product.html">Fashion Wear White</a>
                                                <div class="price">$50</div>
                                            </div>
                                            <a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
                                        </li>
                                        <li>
                                            <a class="cart-mini-lft" href="product.html"><img src="{{url('front-theme/images/shop/pro-3.jpg')}}" alt=""></a>
                                            <div class="cart-body">
                                                <a href="product.html">Ultimate Fashion</a>
                                                <div class="price">$220</div>
                                            </div>
                                            <a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
                                        </li>
                                    </ul>
                                    <div class="mini-cart-total">
                                        <div class="clearfix">
                                            <div class="left">Sub-Total:</div>
                                            <div class="right">$230.00</div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="left">Tax (-10.00):</div>
                                            <div class="right">$12.05</div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="clearfix">
                                            <div class="left">Total:</div>
                                            <div class="right">$200.20</div>
                                        </div>
                                    </div>
                                    <div class="checkout">
                                        <a class="btn" href="cart.html">View Cart</a><a class="btn" href="check-out.html">Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sectionWrapper">
        <div class="container">
            <div class="row">
                <div class="box success-box center hidden">Your item was added succesfully.</div>
                <div class="clearfix"></div>
                <aside class="cell-3 left-shop">
                    @include('frontend.extra.product_left_bar')
                </aside>
                <!-- cell6  class="proButton modal-trigger" data-modal="modal-name" -->
                <div class="cell-6">
                    <div class="button-group">
                        <a class="btn btn-primary modal-trigger online-smart-way model-restaurant-btn fx animated fadeInUp" href="#" data-animate="fadeInUp" data-animation-delay="600" data-modal="delivery_time_detail" style="animation-delay: 600ms;">
                            <span><i class="fa fa-bus selectedI"></i>Delivery Time</span> </a>

                        <a class="btn btn-primary modal-trigger online-smart-way model-restaurant-btn fx animated fadeInUp" href="#" data-animate="fadeInUp" data-modal="alergy_alert" data-animation-delay="600" style="animation-delay: 600ms;">
                            <span><i class="fa fa-bus selectedI"></i>Allergy Alert</span> </a>

                        <a class="btn btn-primary modal-trigger online-smart-way model-restaurant-btn  fx animated fadeInUp" href="#" data-modal="delivery_detail_info" data-animate="fadeInUp" data-animation-delay="600" style="animation-delay: 600ms;">
                            <span><i class="fa fa-bus selectedI"></i>Delivery Info</span> </a>
                    </div>
                    
                    @if(isset($orderINfoText))
                        <div class="modal" id="delivery_time_detail" style="z-index: 9999;">
                            <div class="modal-sandbox"></div>
                            <div class="modal-box">
                                <div class="modal-header">
                                    <div class="close-modal">✖</div> 
                                    <h4><i class="fa fa-info-circle"></i> Opening information</h4>
                                </div>
                                <div class="modal-body" style="padding:20px 10px 10px 10px;">       
                                    <div class="cell-12">
                                       {!!html_entity_decode($orderINfoText->delivery_time)!!} 
                                    </div>               
                                </div>
                            </div>
                        </div>

                        <div class="modal" id="alergy_alert" style="z-index: 9999;">
                            <div class="modal-sandbox"></div>
                            <div class="modal-box">
                                <div class="modal-header">
                                    <div class="close-modal">✖</div> 
                                    <h4><i class="fa fa-info-circle"></i> Allergy & dietary information</h4>
                                </div>
                                <div class="modal-body" style="padding:20px 10px 10px 10px;">       
                                    <div class="cell-12">
                                       {!!html_entity_decode($orderINfoText->allergy_alert)!!} 
                                    </div>               
                                </div>
                            </div>
                        </div>

                        <div class="modal" id="delivery_detail_info" style="z-index: 9999;">
                            <div class="modal-sandbox"></div>
                            <div class="modal-box">
                                <div class="modal-header">
                                    <div class="close-modal">✖</div> 
                                    <h4><i class="fa fa-info-circle"></i> Delivery Info</h4>
                                </div>
                                <div class="modal-body" style="padding:20px 10px 10px 10px;">       
                                    <div class="cell-12">
                                       {!!html_entity_decode($orderINfoText->delivery_info)!!} 
                                    </div>               
                                </div>
                            </div>
                        </div>

                    

                    <!-- button -->
                    <div class="clearfix"></div>
                    <div class="padd-top-20"></div>
                    <div class="cell-12">
                        <blockquote>
                            {!!html_entity_decode($orderINfoText->description)!!} 
                        </blockquote>
                    </div>
                    @endif
                    <div class="clearfix"></div>
                    <div class="padd-top-20"></div>
                    <!-- NOTE -->
                    
                    @include('frontend.extra.ros_menu')
                    

                </div>


                @include('frontend.extra.cart')



            </div>
        </div>
    </div>
@endsection	





@section('css')
    <link rel="stylesheet" href="{{url('front-theme/css/custom.css')}}">
    <link rel="stylesheet" href="{{url('front-theme/css/radio-button/style.css')}}">
    <style type="text/css">
        .display-none-sec
        {
            display: none !important;
        }

        .discount-space
        {
            display: none;
        }

        .cart-item-highlight
        {
            font-weight: 600;
        }

        .cart-data-mini-box
        {
            width: 100%;
            display: block; 
        }

        .cart-price-mini-box
        {
            display:inline-grid;
            font-weight: 600;
        }
        .cart-price-mini-box > span::before 
        {
            content:"$";
            display:inline-grid;
            font-weight: 600;
        }
        .cart-extra-mini-box
        {
            width: 150px;
            display:inline-grid;
            overflow: hidden;
            font-weight: bold;
        }
    </style>
@endsection

@section('slider-js')
    <script type="text/javascript" src="{{url('front-theme/js/jquery.animateNumber.min.js')}}"></script>
    <script type="text/javascript" src="{{url('front-theme/js/jquery.easypiechart.min.js')}}"></script>

    <script src="{{url('front-theme/js/sweetalert.min.js')}}"></script>
	@include('frontend.extra.cart-js')
@endsection
