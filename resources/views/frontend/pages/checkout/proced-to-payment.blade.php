@extends('frontend.layout.master')
@section('title','Proced To Payment')
@section('content')
<div id="contentWrapper">
	<div class="page-title title-1">
		<div class="container">
			<div class="row">
				<div class="cell-12">
					<h1 class="fx" data-animate="fadeInLeft">Check <span>out</span></h1>
					<div class="breadcrumbs main-bg fx" data-animate="fadeInUp">
						<span class="bold">You Are In:</span><a href="#">Home</a><span class="line-separate">/</span><a href="#">Shop </a><span class="line-separate">/</span><span>Check out</span>
					</div>
					<div class="cart-icon fx" data-animate="fadeInRight">
						<div class="cart-heading">
							<i class="fa fa-shopping-cart"></i><span id="cart-count"><b>0</b> item(s)</span>
						</div>
						<div class="cart-popup">
							<div class="empty">Your shopping cart is empty!</div>
							<div class="mini-cart">
								<ul class="mini-cart-list">
									<li>
										<a class="cart-mini-lft" href="product.php"><img src="images/shop/pro-1.jpg" alt=""></a>
										<div class="cart-body">
											<a href="product.php">Ultimate Fashion Wear White</a>
											<div class="price">$150</div>
										</div>
										<a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
									</li>
									<li>
										<a class="cart-mini-lft" href="product.php"><img src="images/shop/pro-2.jpg" alt=""></a>
										<div class="cart-body">
											<a href="product.php">Fashion Wear White</a>
											<div class="price">$50</div>
										</div>
										<a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
									</li>
									<li>
										<a class="cart-mini-lft" href="product.php"><img src="images/shop/pro-3.jpg" alt=""></a>
										<div class="cart-body">
											<a href="product.php">Ultimate Fashion</a>
											<div class="price">$220</div>
										</div>
										<a class="remove" href="#"><i class="fa fa-times" title="Remove"></i></a>
									</li>
								</ul>
								<div class="mini-cart-total">
									<div class="clearfix">
										<div class="left">Sub-Total:</div>
										<div class="right">$230.00</div>
									</div>
									<div class="clearfix">
										<div class="left">Tax (-10.00):</div>
										<div class="right">$12.05</div>
									</div>
									<div class="clearfix">
									</div>
									<div class="clearfix">
										<div class="left">Total:</div>
										<div class="right">$200.20</div>
									</div>
								</div>
								<div class="checkout">
									<a class="btn" href="cart.php">View Cart</a><a class="btn" href="check-out.php">Checkout</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="sectionWrapper">
		<div class="container">
			<div class="steps">
				<ul>
					<li><span>1 - Shopping cart</span></li>
					<li class="selected"><span>2 - Delivery Address</span></li>
					<li><span>3 - Payment Method</span></li>
					<li><span>4 - Confirm Payment</span></li>
				</ul>
			</div>
			<div class="row">
				<form action="javascript:void(0);" id="reg_form" class="contact-form">
					<div class="cell-6">
    					<div class="form-input">
    						<label>Full Name<span class="red">*</span></label>
    						<input name="name" value="{{Auth::user()->name}}" type="text" required>
    					</div>
    					
    					<div class="form-input">
	    					<label>Address <span class="red">*</span></label>
	    					<input name="address" type="text" required>
	    				</div>

					</div>
					<div class="cell-6">
						<div class="form-input">
    						<label>Phone<span class="red">*</span></label>
    						<input name="phone" type="text" required>
    					</div>
						<div class="form-input">
    						<label>Email<span class="red">*</span></label>
    						<input name="email" value="{{Auth::user()->email}}" type="email" required>
    					</div>
					</div>

				</form>
				
			</div>
			<div class="clearfix"></div>
			<div class="continue-btn">
				<a class="btn btn-medium main-bg right save-delivery-info" href="javascript:void(0);">			Continue
				</a>
				<a class="btn btn-medium left" href="{{url('shopping-cart')}}">Back</a>
			</div>
		</div>
	</div>
	
</div>
@endsection
@section('slider-js')
	<script type="text/javascript">
		$(document).ready(function(){

			//------------------------Ajax Customer Start-------------------------//
		         var AddDeliveryUrl="{{url('order-item/add-to-cart/json')}}";
		         var data={'_token':"{{csrf_token()}}"};
		         $.ajax({
		            'async': true,
		            'type': "GET",
		            'global': false,
		            'dataType': 'json',
		            'url': AddDeliveryUrl,
		            'data': data,
		            'success': function (data) {
		                console.log("Delivery JSON Address : "+data);
		                if(data.deliveryDetail)
		                {
		                	var obj=data.deliveryDetail;
		                	console.log(obj);
		                	$("input[name=name]").val(obj.name);
							$("input[name=phone]").val(obj.phone);
							$("input[name=address]").val(obj.address);
							$("input[name=email]").val(obj.email);
		                }
		                
		            }
		        });
		        //------------------------Ajax Customer End---------------------------//
		        


			$(".save-delivery-info").click(function(){
				var name=$("input[name=name]").val();
				var phone=$("input[name=phone]").val();
				var address=$("input[name=address]").val();
				var email=$("input[name=email]").val();
				
				if(name.length==0){ alert("Name should not be empty."); return false; }
				if(phone.length==0){ alert("Phone should not be empty."); return false; }
				if(address.length==0){ alert("Address should not be empty."); return false; }
				if(email.length==0){ alert("Email address should not be empty."); return false; }
				

		        //------------------------Ajax Customer Start-------------------------//
		         var payment_method="{{url('payment-method')}}";
		         var AddHowMowKhaoUrl="{{url('add-to-cart/delivery-address')}}";
		         var data={'_token':"{{csrf_token()}}",'name':name,'phone':phone,'address':address,'email':email};
		         $.ajax({
		            'async': true,
		            'type': "POST",
		            'global': false,
		            'dataType': 'json',
		            'url': AddHowMowKhaoUrl,
		            'data': data,
		            'success': function (data) {
		                console.log("Delivery Address : "+data);
		                if(data==1)
		                {
		                	window.location.href=payment_method;
		                }
		            }
		        });
		        //------------------------Ajax Customer End---------------------------//

			});
		});
	</script>
@endsection