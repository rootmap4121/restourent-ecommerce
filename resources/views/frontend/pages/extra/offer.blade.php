@extends('frontend.layout.master')
@section('title','Our Offer')
@section('content')	
    <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="cell-12">
                    <h1 class="fx" data-animate="fadeInLeft">Today  <span>Offer</span></h1>
                    <div class="breadcrumbs main-bg fx" data-animate="fadeInUp">
                        <span class="bold">You Are In:</span><a href="#">Home</a><span class="line-separate">/</span><a href="#">Pages </a><span class="line-separate">/</span><span>Offer</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sectionWrapper">
        <div class="container">
            <div class="row">
                @if(isset($offer))
                @foreach($offer as $off)
                <div class="cell-3">
                    <div class="material-card-content" style="border-top-color: rgb(246, 187, 66);">

                        <h4 class="material-card-title">{{$off->title}}</h4>
                        <p class="material-card-summary">({{$off->name}})</p>
                        <hr>                                    
                        <p class="material-card-summary"><?= html_entity_decode($off->description);?></p>
                        <img class="cover" alt="" src="{{ URL::asset('upload/today-offer/'.$off->offerimage) }}">
                    </div>
                </div>
                @endforeach
                @endif
                {{-- <div class="cell-3">
                    <div class="material-card-content" style="border-top-color: rgb(246, 187, 66);">

                        <h4 class="material-card-title">SUNDAY SPECIAL BUFFET LUNCH</h4>
                        <p class="material-card-summary">(Dine in Only)</p>
                        <hr>                                    
                        <p class="material-card-summary">Help yourself to a wonderful selections of starters, selections of main dishes, side vegetables, rice &amp; bread</p>
                        <img class="cover" alt="" src="{{url('front-theme/images/sunday-buffet-offer.jpg')}}">
                    </div>
                </div>
                <div class="cell-3">
                    <div class="material-card-content" style="border-top-color: rgb(246, 187, 66);">

                        <h4 class="material-card-title">SUNDAY SPECIAL BUFFET LUNCH</h4>
                        <p class="material-card-summary">(Dine in Only)</p>
                        <hr>                                    
                        <p class="material-card-summary">Help yourself to a wonderful selections of starters, selections of main dishes, side vegetables, rice &amp; bread</p>
                        <img class="cover" alt="" src="{{url('front-theme/images/sunday-buffet-offer.jpg')}}">
                    </div>
                </div>
                <div class="cell-3">
                    <div class="material-card-content" style="border-top-color: rgb(246, 187, 66);">

                        <h4 class="material-card-title">SUNDAY SPECIAL BUFFET LUNCH</h4>
                        <p class="material-card-summary">(Dine in Only)</p>
                        <hr>                                    
                        <p class="material-card-summary">Help yourself to a wonderful selections of starters, selections of main dishes, side vegetables, rice &amp; bread</p>
                        <img class="cover" alt="" src="{{url('front-theme/images/sunday-buffet-offer.jpg')}}">
                    </div>
                </div> --}}
                



            </div>
        </div>
    </div>
@endsection	





@section('css')
    <link rel="stylesheet" href="{{url('front-theme/css/custom.css')}}">
    
@endsection

